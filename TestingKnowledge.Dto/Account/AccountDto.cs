﻿using System;

namespace TestingKnowledge.Dto
{
    public class AccountDto : AccountSecureDto
    {
        public string Password { get; set; }

        public AccountDto( Guid domainId, string login, string password, AccountRole role )
            :   base( domainId, login, role )
        {
            Password = password;
        }
    }
}
