﻿using System;
using System.Collections.Generic;

namespace TestingKnowledge.Dto
{
    public class AccountSecureDto : DomainEntityDto<AccountSecureDto>
    {
        public string Login { get; private set; }

        public AccountRole Role { get; private set; }

        public AccountSecureDto( Guid domainId, string login, AccountRole role )
            :   base( domainId )
        {
            Login = login;
            Role = role;
        }
        
        protected override IEnumerable < object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List < object > { DomainId, Login, Role };
        }
    }
}
