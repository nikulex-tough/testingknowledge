﻿namespace TestingKnowledge.Dto
{
    public enum AccountRole
    {
        User = 1,
        Moderator = 2,
        Admin = 4
    }
}
