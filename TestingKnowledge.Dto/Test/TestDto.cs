﻿using System;
using System.Collections.Generic;

namespace TestingKnowledge.Dto
{
    public class TestDto : DomainEntityDto < TestDto >
    {
        public string Title { get; private set; }
        public string Description { get; private set; }
        public Guid CreatorId { get; private set; }
        public int CountOfTests { get; private set; }
        public int CountGenerate { get; private set; }
        public float MaxMark { get; private set; }

        public IList < QuestionGroupDto > QuestionGroups { get; private set; }

        public TestDto(
                Guid domainId
            ,   string title
            ,   string description
            ,   Guid creatorId
            ,   int countOfTests
            ,   int countGenerate
            ,   float maxMark
            ,   IList < QuestionGroupDto > questionGroups
        )
            :   base( domainId )
        {
            Title = title;
            Description = description;
            CreatorId = creatorId;
            CountOfTests = CountOfTests;
            CountGenerate = countGenerate;
            MaxMark = maxMark;
            QuestionGroups = questionGroups;
        }

        protected override IEnumerable < object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List < object > { DomainId, QuestionGroups };
        }
    }
}
