﻿using System;
using System.Collections.Generic;

namespace TestingKnowledge.Dto
{
    public class QuestionDto : DomainEntityDto < QuestionDto >
    {
        public string Text { get; }

        public IList < OptionDto > Options { get; }

        public Guid QuestionGroupId { get; set; }

        public QuestionDto(
                Guid domainId
            ,   Guid questionGroupId
            ,   string text
            ,   IList < OptionDto > options
        )
            :   base( domainId )
        {
            QuestionGroupId = questionGroupId;
            Text = text;
            Options = options;
        }
        
        protected override IEnumerable < object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List < object > { DomainId, Text, Options, QuestionGroupId };
        }
    }
}
