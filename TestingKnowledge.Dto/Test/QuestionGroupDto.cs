﻿using System;
using System.Collections.Generic;

namespace TestingKnowledge.Dto
{
    public class QuestionGroupDto : DomainEntityDto < QuestionDto >
    {
        public Guid TestId { get; }

        public float MarkOfOneQuestion { get; }
        public float MaxMark { get; }
        public int CountGenerate { get; }
        public IList < QuestionDto > Questions { get; private set; }

        public QuestionGroupDto(
                Guid domainId
            ,   Guid testId
            ,   IList < QuestionDto > questions
            ,   float markOfOneQuestion
            ,   float maxMark
            ,   int countGenerate
        )
            :   base( domainId )
        {
            TestId = testId;
            Questions = questions;
            MarkOfOneQuestion = markOfOneQuestion;
            MaxMark = maxMark;
            CountGenerate = countGenerate;
        }

        protected override IEnumerable < object > GetAttributesToIncludeInEqualityCheck()
        {
            return new List < object > { DomainId, MarkOfOneQuestion, MaxMark, CountGenerate, Questions };
        }
    }
}
