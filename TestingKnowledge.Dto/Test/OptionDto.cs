﻿using System;
using System.Collections.Generic;

namespace TestingKnowledge.Dto
{
    public class OptionDto : DomainEntityDto < OptionDto >
    {
        public Guid QuestionId { get; }

        public string Text { get; }

        public OptionDto( Guid domainId, Guid questionId, string text )
            :   base( domainId )
        {
            QuestionId = questionId;
            Text = text;
        }

        protected override IEnumerable < object > GetAttributesToIncludeInEqualityCheck()
        {
            return new List < object > { DomainId, Text, QuestionId };
        }
    }
}