﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace TestingKnowledge.Dto
{
    public abstract class DomainEntityDto < TConcreteDto > : Utils.Value < DomainEntityDto < TConcreteDto > >
    {
        public Guid DomainId { get; private set; }

        protected DomainEntityDto ( Guid domainId )
        {
            DomainId = domainId;
        }
    }
}