﻿using System;
using System.Collections.Generic;

namespace TestingKnowledge.Dto
{
    public class TestSessionDto : DomainEntityDto < TestSessionDto >
    {
        public Guid TestId { get; private set; }
        public Guid AccountId { get; private set; }
        public float Mark { get; private set; }
        public float MaxMark { get; private set; }
        public bool IsFinished { get; private set; }
        public IList < Guid > Questions { get; private set; }

        public TestSessionDto(
                Guid domainId
            ,   Guid testId
            ,   Guid accountId
            ,   float mark
            ,   float maxMark
            ,   bool isFinished
            ,   IList < Guid > questions
        )
            :   base( domainId )
        {
            TestId = testId;
            AccountId = accountId;
            Mark = mark;
            MaxMark = maxMark;
            IsFinished = isFinished;
            Questions = questions;
        }

        protected override IEnumerable < object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List < object > { DomainId, TestId, Mark, MaxMark, IsFinished, Questions };
        }
    }
}
