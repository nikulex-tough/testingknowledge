﻿using System;
using System.Collections.Generic;

namespace TestingKnowledge.Dto
{
    public class TestSessionQuestionDto : DomainEntityDto < TestSessionQuestionDto >
    {
        public Guid SessionId { get; }
        public QuestionDto Question { get; private set; }
        public ICollection< string > Answer { get; private set; }
        public float Mark { get; private set; }
        public float MaxMark { get; private set; }

        public TestSessionQuestionDto(
                Guid domainId
            ,   Guid sessionId
            ,   QuestionDto question
            ,   ICollection< string > answer
            ,   float mark
            ,   float maxMark
        )
            :   base( domainId )
        {
            SessionId = sessionId;
            Question = question;
            Answer = answer;
            Mark = mark;
            MaxMark = maxMark;
        }

        protected override IEnumerable < object > GetAttributesToIncludeInEqualityCheck()
        {
            return new List < object > { DomainId, SessionId, Question, Answer, Mark, MaxMark };
        }
    }
}
