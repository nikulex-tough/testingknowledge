﻿using System;
using System.Data.Entity;
using System.Linq;
using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.EntityFramework
{
    public class TestSessionRepository : BasicRepository < TestSession >, ITestSessionRepository
    {
        public TestSessionRepository( TestingKnowledgeDbContext dbContext )
            :   base( dbContext, dbContext.TestSessions )
        {
        }

        public IQueryable < TestSession > FindByAccount( Account account )
        {
           return GetDBSet().Where( ( ts ) => ts.Account == account );
        }

        public IQueryable < TestSession > FindByTest( Test test )
        {
            return GetDBSet().Where( ( ts ) => ts.Test == test );
        }

        public IQueryable < Guid > SelectActiveIds()
        {
            return GetDBSet().Where( ( ts ) => ts.IsFinished == false ).Select( ( ts ) => ts.DomainId );
        }

        public IQueryable< Guid > SelectFinishedIds()
        {
            return GetDBSet().Where( ( ts ) => ts.IsFinished == true ).Select( ( ts ) => ts.DomainId );
        }

        public IQueryable< Guid > SelectPassedIds( float minMark )
        {
            return GetDBSet().Where( ( ts ) => ts.Mark >= minMark ).Select( ( ts ) => ts.DomainId );
        }

        public IQueryable< Guid > SelectNotPassedIds( float minMark )
        {
            return GetDBSet().Where( ( ts ) => ts.Mark < minMark ).Select( ( ts ) => ts.DomainId );
        }
    }
}
