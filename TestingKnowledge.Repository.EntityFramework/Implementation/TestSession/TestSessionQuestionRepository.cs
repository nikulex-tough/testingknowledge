﻿using System;
using System.Linq;

using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.EntityFramework
{
    public class TestSessionQuestionRepository : BasicRepository < TestSessionQuestion >, ITestSessionQuestionRepository
    {
        public TestSessionQuestionRepository( TestingKnowledgeDbContext dbContext )
            :   base( dbContext, dbContext.TestSessionQuestions )
        {
        }

        public IQueryable< Guid > FindByQuestion( Question question )
        {
            return GetDBSet()
                .Where( sq => sq.Question.DomainId == question.DomainId )
                .Select( sq => sq.DomainId );
        }

        public TestSessionQuestion FindBySessionAndQuestion( TestSession session, Question question )
        {
            return GetDBSet().FirstOrDefault(
                sq => sq.Question.DomainId == question.DomainId
                && sq.Session.DomainId == session.DomainId
            );
        }
    }
}