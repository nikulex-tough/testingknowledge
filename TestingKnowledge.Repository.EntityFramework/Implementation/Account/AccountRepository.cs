﻿using System.Linq;
using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.EntityFramework
{
    public class AccountRepository : BasicRepository < Account >, IAccountRepository
    {
        public AccountRepository( TestingKnowledgeDbContext dbContext )
            :   base( dbContext, dbContext.Accounts )
        {
        }

        public Account FindByLogin( string login )
        {
            return GetDBSet().Where( ( a ) => a.Login == login ).FirstOrDefault();
        }
    }
}
