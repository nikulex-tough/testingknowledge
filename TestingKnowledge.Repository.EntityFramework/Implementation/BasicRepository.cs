﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Data.Entity;
using System.Linq;

namespace TestingKnowledge.Repository.EntityFramework
{
    public abstract class BasicRepository < T > where T : Utils.Entity
    {
        protected BasicRepository( TestingKnowledgeDbContext dbContext, DbSet < T > dbSet )
        {
            this.dbContext = dbContext;
            this.dbSet = dbSet;
        }

        protected TestingKnowledgeDbContext GetDBContext()
        {
            return dbContext;
        }

        protected DbSet < T > GetDBSet()
        {
            return dbSet;
        }

        public void Add( T obj )
        {
            dbSet.Add( obj );
        }

        public void Delete( T obj )
        {
            dbSet.Remove( obj );
        }

        public IQueryable < T > LoadAll()
        {
            return dbSet;
        }

        public int Count()
        {
            return dbSet.Count();
        }

        public T FindByDomainId( Guid domainId )
        {
            return dbSet.Where( e => e.DomainId == domainId ).SingleOrDefault();
        }


        public IQueryable < Guid > SelectAllDomainIds()
        {
            return dbSet.Select( e => e.DomainId );
        }

        public void StartTransaction()
        {
            dbContext.Database.BeginTransaction();
        }

        public void Commit()
        {
            dbContext.ChangeTracker.DetectChanges();
            dbContext.SaveChanges();
            dbContext.Database.CurrentTransaction.Commit();
        }

        public void Rollback()
        {
            dbContext.Database.CurrentTransaction.Rollback();
        }


        private TestingKnowledgeDbContext dbContext;
        private DbSet < T > dbSet;
    }
}
