﻿using System;
using System.Data.Entity;
using System.Linq;
using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.EntityFramework
{
    public class TestRepository : BasicRepository < Test >, ITestRepostory
    {
        public TestRepository( TestingKnowledgeDbContext dbContext )
            :   base( dbContext, dbContext.Tests )
        {
        }

        public IQueryable < Guid > FindByCreator( Account account )
        {
            return GetDBSet()
                .Where( ( t ) => t.Creator.DomainId == account.DomainId )
                .Select( ( t ) => t.DomainId );
        }

        public Test FindByTitle( string title )
        {
            return GetDBSet().Where( ( t ) => t.Title == title ).FirstOrDefault ();
        }
    }
}
