﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.EntityFramework
{
    public class QuestionGroupRepository : BasicRepository < QuestionGroup >, IQuestionGroupRepository
    {
        public QuestionGroupRepository( TestingKnowledgeDbContext dbContext )
            :   base( dbContext, dbContext.QuestionGroups )
        {
        }
    }
}