﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.EntityFramework
{
    public class QuestionRepository : BasicRepository < Question >, IQuestionRepository
    {
        public QuestionRepository( TestingKnowledgeDbContext dbContext )
            :   base( dbContext, dbContext.Questions )
        {
        }
    }
}