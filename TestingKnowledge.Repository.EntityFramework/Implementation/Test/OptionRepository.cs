﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.EntityFramework
{
    public class OptionRepository : BasicRepository < Option >, IOptionRepository
    {
        public OptionRepository( TestingKnowledgeDbContext dbContext )
            :   base( dbContext, dbContext.Options )
        {
        }
    }
}