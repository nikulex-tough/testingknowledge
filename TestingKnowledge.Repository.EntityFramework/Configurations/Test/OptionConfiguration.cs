﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.EntityFramework.Configurations
{
    class OptionConfiguration : BasicEntityConfiguration < Option >
    {
        public OptionConfiguration()
        {
            HasRequired( ( ao ) => ao.Question );
            Property( ao => ao.Text ).IsRequired();
            Property( ao => ao.IsRight ).IsRequired();
        }
    }
}
