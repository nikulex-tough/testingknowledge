﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.EntityFramework.Configurations
{
    class TestConfiguration : BasicEntityConfiguration < Test >
    {
        public TestConfiguration()
        {
            HasRequired( ( t ) => t.Creator );
            HasMany( ( t ) => t.QuestionGroups ).WithOptional();
            Property( ( t ) => t.Title ).IsRequired();
            Property( ( t ) => t.Description ).IsRequired();
        }
    }
}
