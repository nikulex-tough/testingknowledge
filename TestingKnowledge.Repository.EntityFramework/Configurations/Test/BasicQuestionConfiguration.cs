﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.EntityFramework.Configurations
{
    class BasicQuestionConfiguration < T > : BasicEntityConfiguration < T >
        where T : Question
    {
        public BasicQuestionConfiguration()
        {
            HasRequired( ( qb ) => qb.Group );
            Property( ( qb ) => qb.Text ).IsRequired();
        }
    }
}
