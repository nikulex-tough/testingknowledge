﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.EntityFramework.Configurations
{
    class QuestionSelectConfiguration : BasicQuestionConfiguration< QuestionSelect >
    {
        public QuestionSelectConfiguration()
        {
            HasMany( ( qs ) => qs.Options ).WithOptional();
        }
    }
}
