﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.EntityFramework.Configurations
{
    class QuestionGroupConfiguration : BasicEntityConfiguration < QuestionGroup >
    {
        public QuestionGroupConfiguration()
        {
            HasRequired( ( qg ) => qg.Test );
            HasMany( ( qg ) => qg.Questions ).WithOptional();
            Property( ( qg ) => qg.CountGenerate ).IsRequired();
            Property( ( qg ) => qg.MarkOfOneQuestion ).IsRequired();
        }
    }
}
