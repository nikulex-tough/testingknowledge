﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.EntityFramework.Configurations
{
    class QuestionInputConfiguration : BasicQuestionConfiguration < QuestionInput >
    {
        public QuestionInputConfiguration()
        {
            Property( ( qi ) => qi.RightAnswer ).IsRequired();
        }
    }
}
