﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System.Data.Entity.ModelConfiguration;

namespace TestingKnowledge.Repository.EntityFramework.Configurations
{
    abstract class BasicEntityConfiguration < T > : EntityTypeConfiguration < T > 
        where T : Utils.Entity
    {
        protected BasicEntityConfiguration ()
        {
            HasKey( e => e.DatabaseId );
            Property( e => e.DomainId ).IsRequired();
        }
    }
}
