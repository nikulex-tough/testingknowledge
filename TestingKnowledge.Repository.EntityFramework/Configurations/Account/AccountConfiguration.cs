﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.EntityFramework.Configurations
{
    class AccountConfiguration : BasicEntityConfiguration < Account >
    {
        public AccountConfiguration()
        {
            Property( a => a.Login ).IsRequired();
            Property( a => a.Password ).IsRequired();
        }
    }
}
