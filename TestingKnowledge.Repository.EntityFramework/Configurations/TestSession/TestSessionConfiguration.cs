﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.EntityFramework.Configurations
{
    class TestSessionConfiguration : BasicEntityConfiguration < TestSession >
    {
        public TestSessionConfiguration()
        {
            HasMany( ( ts ) => ts.SessionQuestions ).WithOptional();
            HasOptional( ( ts ) => ts.Account );
            HasRequired( ( ts ) => ts.Test );
            Property( ( ts ) => ts.IsFinished ).IsOptional();
        }
    }
}
