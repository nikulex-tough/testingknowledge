﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.EntityFramework.Configurations
{
    class TestSessionQuestionConfiguration : BasicEntityConfiguration < TestSessionQuestion >
    {
        public TestSessionQuestionConfiguration()
        {
            HasRequired( ( tsq ) => tsq.Session );
            HasRequired( ( tsq ) => tsq.Question );
            Property( ( tsq ) => tsq.Answer ).IsOptional();
        }
    }
}
