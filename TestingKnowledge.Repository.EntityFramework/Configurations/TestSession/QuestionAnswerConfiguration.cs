﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.EntityFramework.Configurations
{
    class QuestionAnswerConfiguration : BasicEntityConfiguration< QuestionAnswer >
    {
        public QuestionAnswerConfiguration()
        {
            Property( q => q.Value ).IsRequired();
        }
    }
}