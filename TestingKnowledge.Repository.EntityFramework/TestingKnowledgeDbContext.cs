﻿using TestingKnowledge.Model;

using System.Data.Entity;
using System;

namespace TestingKnowledge.Repository.EntityFramework
{
    public class TestingKnowledgeDbContext : DbContext, ITransactionDbContext
    {
        static TestingKnowledgeDbContext()
        {
            Database.SetInitializer(
                new DropCreateDatabaseAlways < TestingKnowledgeDbContext > ()
            );
        }

        public TestingKnowledgeDbContext()
        {
            Database.Log = ( s => System.Diagnostics.Debug.WriteLine( s ) );
        }

        public DbSet < Account > Accounts { get; set; }
        
        public DbSet < Test > Tests { get; set; }
        public DbSet < QuestionGroup > QuestionGroups { get; set; }
        public DbSet < Question > Questions { get; set; }
        public DbSet < Option > Options { get; set; }

        public DbSet < TestSession > TestSessions { get; set; }
        public DbSet < TestSessionQuestion > TestSessionQuestions { get; set; }

 
        protected override void OnModelCreating( DbModelBuilder modelBuilder )
        {
            modelBuilder.Configurations.Add( new Configurations.AccountConfiguration() );

            modelBuilder.Configurations.Add( new Configurations.OptionConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.QuestionGroupConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.QuestionConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.QuestionInputConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.QuestionSelectConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.TestConfiguration() );

            modelBuilder.Configurations.Add( new Configurations.TestSessionConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.TestSessionQuestionConfiguration() );
        }

        public void BeginTransaction()
        {
            Database.BeginTransaction();
        }

        public void Commit()
        {
            SaveChanges();
            Database.CurrentTransaction.Commit();
        }

        public void Rollback()
        {
            Database.CurrentTransaction.Rollback();
        }
    }
}
