﻿using System;
using System.Collections.Generic;

namespace TestingKnowledge.Model
{
    public class QuestionGroup : Utils.Entity
    {
        public Test Test { get; set; }
        public void Free() => Test?.Delete( this );

        public virtual ICollection < Question > Questions { get; set; } = new List < Question > ();

        public void Add( Question question )
        {
            if ( question == null )
                throw new ArgumentNullException( "question" );
            if ( Questions.Contains( question ) )
                throw new ArgumentOutOfRangeException( "Question allready exist", "question" );
            Questions.Add ( question );
            question.Group = this;
        }

        public void Delete( Question question )
        {
            if ( question == null )
                throw new ArgumentNullException( "question" );
            if ( !Questions.Contains( question ) )
                throw new ArgumentOutOfRangeException( "Question not exist", "question" );
            Questions.Remove( question );
            question.Group = null;
        }

        public float MarkOfOneQuestion { get; set; }
        
        public int CountGenerate { get; set; }

        public float MaxMark => MarkOfOneQuestion * CountGenerate;

        private Random random = new Random();
        public List < Question > Generate()
        {
            if ( Questions.Count == 0 )
                throw new InvalidOperationException( "Not initialized collection" );
            if ( CountGenerate == 0 )
                throw new InvalidOperationException( "Count generate was not set" );

            List < Question > sessionQuestions = new List < Question > ( Questions );
            for ( int i = 0; i < Questions.Count - CountGenerate; ++i )
                sessionQuestions.RemoveAt( random.Next ( sessionQuestions.Count ) );
            return sessionQuestions;
        }

        public QuestionGroup () {}

        public QuestionGroup( Guid domainId, float markOfOneQuestion )
            :   base( domainId )
        {
            MarkOfOneQuestion = markOfOneQuestion;
        }
        
        public int CompareTo ( QuestionGroup other )
        {
            if ( MarkOfOneQuestion < other.MarkOfOneQuestion )
                return -1;
            else if ( MarkOfOneQuestion > other.MarkOfOneQuestion )
                return 1;
            return 0;
        }
    }
}
