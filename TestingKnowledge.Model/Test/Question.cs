﻿using System;

namespace TestingKnowledge.Model
{
    public abstract class Question : Utils.Entity
    {
        public string Text { get; set; } = "text"; 
        
        public QuestionGroup Group { get; set; }
        public void Free() => Group?.Delete( this );

        public Question () {}

        protected Question ( Guid domainId, string text )
            :   base( domainId )
        {
            Text = text;
        }

        public abstract void Accept ( IQuestionVisitor visitor );
    }
}
