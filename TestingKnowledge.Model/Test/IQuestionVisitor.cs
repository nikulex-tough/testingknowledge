﻿namespace TestingKnowledge.Model
{
    public interface IQuestionVisitor
    {
        void Visit ( QuestionInput question );
        void Visit ( QuestionSelect question );
    }
}