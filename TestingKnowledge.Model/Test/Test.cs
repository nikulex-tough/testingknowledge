﻿using System;
using System.Collections.Generic;

using TestingKnowledge.Utils;

namespace TestingKnowledge.Model
{
    public class Test : Entity
    {
        public virtual ICollection < QuestionGroup > QuestionGroups { get; } = new List < QuestionGroup >();

        public void Add( QuestionGroup questionGroup )
        {
            if ( questionGroup == null )
                throw new ArgumentNullException( "questionGroup" );
            if ( QuestionGroups.Contains( questionGroup ) )
                throw new ArgumentOutOfRangeException( "Question group allready exist", "questionGroup" );
            QuestionGroups.Add ( questionGroup );
            questionGroup.Test = this;
        }

        public void Delete( QuestionGroup questionGroup )
        {
            if ( questionGroup == null )
                throw new ArgumentNullException( "questionGroup" );
            if ( !QuestionGroups.Contains( questionGroup ) )
                throw new ArgumentOutOfRangeException( "Question group not exist", "questionGroup" );
            QuestionGroups.Remove( questionGroup );
            questionGroup.Test = null;
        }

        public string Title { get; set; } = "title";

        public string Description { get; set; } = "description";

        public Account Creator { get; private set; }

        public int CountOfTests
        {
            get
            {
                int count = 0;
                foreach ( QuestionGroup group in QuestionGroups )
                    count += group.Questions.Count;
                return count;
            }
        }

        public int CountGenerate
        {
            get
            {
                int count = 0;
                foreach ( QuestionGroup group in QuestionGroups )
                    count += group.CountGenerate;
                return count;
            }
        }
        

        public Test () {}

        public Test ( Guid domainId, Account creator, string title, string description )
            :   base( domainId )
        {
            Title = title;
            Creator = creator;
            Description = description;
        }

        public float MaxMark
        {
            get
            {
                float summOfMark = 0.0f;
                foreach ( var questionGroup in QuestionGroups )
                    summOfMark += questionGroup.MaxMark;
                return summOfMark;
            }
        }
        
        public List < Question > Generate()
        {
            List < Question > questions = new List< Question > ();

            foreach ( QuestionGroup group in QuestionGroups )
                questions.AddRange ( group.Generate () );
            questions.Shuffle ();

            return questions;
        }
    }
}
