﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TestingKnowledge.Model
{
    public class QuestionSelect : Question
    {
        public virtual ICollection< Option > Options { get; set; } = new List < Option > ();

        public virtual ICollection< Option > RightAnswers => Options.Where( o => o.IsRight ).ToList();

        public void Add( Option option )
        {
            if ( option == null )
                throw new ArgumentNullException( "option" );
            if ( Options.Contains( option ) )
                throw new ArgumentOutOfRangeException( "Option allready exist" );
            Options.Add( option );
            option.Question = this;
        }

        public void Delete( Option option )
        {
            if ( option == null )
                throw new ArgumentNullException( "option" );
            if ( !Options.Contains( option ) )
                throw new ArgumentOutOfRangeException( "Option not exist" );
            Options.Remove( option );
            option.Question = null;
        }

        public QuestionSelect () {}

        public QuestionSelect( Guid domainId, string text )
            :   base( domainId, text )
        {
        }

        public override void Accept( IQuestionVisitor visitor ) => visitor.Visit( this );
    }
}