﻿using System;
using System.Collections.Generic;

namespace TestingKnowledge.Model
{
    public class QuestionInput : Question
    {
        public string RightAnswer { get; set; }

        public QuestionInput () {}

        public QuestionInput ( Guid domainId, string text, string rightAnswer )
            :   base( domainId, text )
        {
            RightAnswer = rightAnswer;
        }

        public override void Accept ( IQuestionVisitor visitor ) => visitor.Visit ( this );
    }
}