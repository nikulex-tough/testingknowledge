﻿using System;

namespace TestingKnowledge.Model
{
    public class Option : Utils.Entity
    {
        public QuestionSelect Question { get; set; }
        public void Free() => Question?.Delete( this );

        public string Text { get; set; } = "option";

        public bool IsRight { get; set; }

        public Option () {}

        public Option ( Guid domainId, string text, bool isRight )
            :   base( domainId )
        {
            Text = text;
            IsRight = isRight;
        }
    }
}
