﻿using System;

namespace TestingKnowledge.Model
{
    public class AccountUser : Account
    {
        public AccountUser () {}

        public AccountUser( Guid domainId, string login, string password )
            :   base( domainId, login, password )
        {
        }

        public override void Accept ( IAccountVisitor visitor ) => visitor.Visit ( this );
    }
}
