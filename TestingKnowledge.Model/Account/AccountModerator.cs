﻿using System;

namespace TestingKnowledge.Model
{
    public class AccountModerator : Account
    {
        public AccountModerator () {}

        public AccountModerator( Guid domainId, string login, string password )
            :   base( domainId, login, password )
        {
        }

        public override void Accept ( IAccountVisitor visitor ) => visitor.Visit ( this );
    }
}
