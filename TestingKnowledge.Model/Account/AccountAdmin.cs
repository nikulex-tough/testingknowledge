﻿using System;

namespace TestingKnowledge.Model
{
    public class AccountAdmin : Account
    {
        public AccountAdmin () {}

        public AccountAdmin( Guid domainId, string login, string password )
            :   base( domainId, login, password )
        {
        }

        public override void Accept ( IAccountVisitor visitor ) => visitor.Visit ( this );
    }
}
