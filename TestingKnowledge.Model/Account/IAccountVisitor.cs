﻿namespace TestingKnowledge.Model
{
    public interface IAccountVisitor
    {
        void Visit ( AccountAdmin account );
        void Visit ( AccountModerator account );
        void Visit ( AccountUser account );
    }
}
