﻿using System;

namespace TestingKnowledge.Model
{
    public abstract class Account : Utils.Entity
    {
        public string Login { get; set; } = "login";
        public string Password { get; set; } = "password";

        public bool CheckPassword( string password ) => Password == password;

        public Account() {}

        public Account( Guid domainId, string login, string password )
            :   base( domainId )
        {
            Login = login;
            Password = password;
        }

        public abstract void Accept ( IAccountVisitor visitor );
    }
}
