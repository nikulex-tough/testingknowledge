﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TestingKnowledge.Model
{
    public class TestSession : Utils.Entity
    {
        public virtual ICollection < TestSessionQuestion > SessionQuestions { get; private set; }
            = new List< TestSessionQuestion >();

        public Account Account { get; set; }

        public Test Test { get; set; }

        public bool IsFinished { get; set; }

        public float Mark
        {
            get
            {
                float mark = 0.0f;
                foreach ( TestSessionQuestion question in SessionQuestions )
                    mark += question.Mark;
                return mark;
            }
        }

        public float MaxMark
        {
            get
            {
                return  SessionQuestions.Count == 0
                    ?   0.0f
                    :   SessionQuestions.First().Question.Group.MaxMark;
            }
        }

        public TestSession() {}

        public TestSession ( Guid domainId, Test test, Account account )
            :   base( domainId )
        {
            Test = test;
            Account = account;
            foreach ( Question question in Test.Generate() )
                SessionQuestions.Add( new TestSessionQuestion( Guid.NewGuid(), question, this ) );
        }
    }
}