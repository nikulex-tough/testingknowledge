﻿using System;
using System.Linq;
using System.Collections.Generic;
using TestingKnowledge.Exceptions;

namespace TestingKnowledge.Model
{
    public class TestSessionQuestion : Utils.Entity
    {
        public TestSession Session { get; set; }

        public Question Question { get; set; }

        public string Answer { get; set; }

        public float Mark
        {
            get
            {
                if ( Question == null )
                    return 0.0f;
                return RightPercent *Question.Group.MarkOfOneQuestion;
            }
        }

        public float MaxMark
        {
            get
            {
                if ( Question == null )
                    return 0.0f;
                return Question.Group.MarkOfOneQuestion;
            }
        }

        public TestSessionQuestion() {}

        public TestSessionQuestion( Guid domainId, Question question, TestSession session = null )
            :   base( domainId )
        {
            Question = question;
            Session = session;
        }


        public void SetAnswer( string input )
        {
            if ( Question is QuestionInput )
                Answer = input;
            else
                throw new QuestionTypeException(
                        typeof( QuestionInput )
                    ,   typeof( QuestionSelect )
                    ,   Question.DomainId
                );
        }

        public void SetAnswer( Option option, bool checkState = true )
        {
            if ( Question is QuestionInput )
                throw new QuestionTypeException(
                        typeof( QuestionSelect )
                    ,   typeof( QuestionInput )
                    ,   Question.DomainId
                );

            QuestionSelect select = Question as QuestionSelect;
            if ( !select.Options.Contains( option ) )
                throw new InQuestionIsNotSuchOptionException( Question.Text, option.Text );

            var answerOptions = AnswerConverter.ToOptions( select, Answer );
            if ( answerOptions.Contains( option ) )
            {
                if ( !checkState )
                    answerOptions.Remove( option );
            }
            else
            {
                if ( checkState )
                    answerOptions.Add( option );
            }
            Answer = AnswerConverter.ToAnswerString( Question, answerOptions );
        }

        private class CollectionConverterVisitor : IQuestionVisitor
        {
            private ICollection < string > _result = new List< string > ();
            private string _answer;

            public static ICollection < string > Get( Question question, string answer )
            {
                CollectionConverterVisitor visitor = new CollectionConverterVisitor();
                visitor._answer = answer;
                question.Accept( visitor );
                return visitor._result;
            }

            public void Visit( QuestionSelect question )
            {
                var options = AnswerConverter.ToOptions( question, _answer );
                foreach ( Option option in options )
                    _result.Add( option.DatabaseId.ToString() );
            }

            public void Visit( QuestionInput question )
            {
                _result.Add( _answer );
            }
        }

        public ICollection< string > AnswerAsColleciton
            => CollectionConverterVisitor.Get( Question, Answer );

        private class RightPercentVisitor : IQuestionVisitor
        {
            private float _result;
            private string _answer;

            public static float Get( Question question, string answer )
            {
                RightPercentVisitor visitor = new RightPercentVisitor();
                visitor._answer = answer;
                question.Accept( visitor );
                return visitor._result;
            }

            public void Visit( QuestionSelect question )
            {
                int countRight = 0;

                var options = AnswerConverter.ToOptions( question, _answer );
                foreach ( Option option in question.Options )
                {
                    bool contains = options.Contains( option );
                    if ( ( contains && option.IsRight ) || ( !contains && !option.IsRight ) )
                        ++countRight;
                }
                _result = countRight / ( float ) question.Options.Count;
            }

            public void Visit( QuestionInput question )
            {
                _result = ( question.RightAnswer.ToUpper() == _answer.ToUpper() ) ? 1.0f : 0.0f;
            }
        }

        public float RightPercent
        {
            get
            {
                if ( Answer == null || Answer.Length == 0 )
                    return 0.0f;
                return RightPercentVisitor.Get( Question, Answer );
            }
        }
    }
}
