﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestingKnowledge.Model
{
    public static class AnswerConverter
    {
        public const char SEPARATOR = '♪';
        
        public static string ToAnswerString( Question question, ICollection< Option > options )
        {
            StringBuilder builder = new StringBuilder();
            foreach ( Option option in options )
                builder.AppendLine( $"{option.DomainId}{SEPARATOR}" );
            return builder.ToString();
        }

        public static ICollection< Option > ToOptions( QuestionSelect question, string answer )
        {
            List< Option > answerList = new List< Option >();

            try
            {
                ICollection< string > parts = answer.Split( SEPARATOR );
                foreach ( string part in parts )
                {
                    if ( part.Length == 0 )
                        continue;

                    Guid partAnswer = Guid.Parse( part );
                    var find = question.Options.Where( ( a ) => a.DomainId == partAnswer );
                    if ( find.Count() == 0 )
                        throw new ArgumentException( "Not exist answer", $"Answer with guid: {partAnswer}" );
                    answerList.Add( find.First() );
                }
            }
            catch
            {
            }

            return answerList;
        }
    }
}
