﻿using TestingKnowledge.Utils;

namespace TestingKnowledge.Model
{
    public class QuestionAnswer : Entity
    {
        public string Value { get; set; }

        public QuestionAnswer() { }

        public QuestionAnswer( string value )
        {
            Value = value;
        }
    }
}
