﻿using System;

namespace TestingKnowledge.TestApp
{
    public class Reporter
    {
        public void WriteLine( string text ) => Console.WriteLine( Indent + text );

        public void WriteLine() => Console.WriteLine();

        public int Level { get; set; }

        public string Indent
        {
            get
            {
                string indent = "";
                for ( int i = 0; i < Level; ++i )
                    indent += "    ";
                return indent;
            }
        }

        public void WriteBodrder( char symbol, int borderSize = 50 )
        {
            for ( int i = 0; i < borderSize; ++i )
                Console.Write( symbol );
            Console.WriteLine();
        }

        public void WriteBodrder( string message, char symbol, int borderSize = 50 )
        {
            int count = ( borderSize - ( message.Length + 2 ) ) / 2;
            for ( int i = 0; i < count; ++i )
                Console.Write( symbol );

            Console.Write( $" {message} " );

            for ( int i = 0; i < count; ++i )
                Console.Write( symbol );
            Console.WriteLine();
        }
    }
}
