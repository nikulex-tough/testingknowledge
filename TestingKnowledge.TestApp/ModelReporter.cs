﻿using System;
using System.Collections.Generic;

using TestingKnowledge.Dto;
using TestingKnowledge.Service;

using Microsoft.Practices.Unity;

namespace TestingKnowledge.TestApp
{
    class ModelReporter : Reporter
    {
        private IAccountService ReportAccountService { get; }
        private ITestEditorService ReportTestService { get; }
        private ITestSessionService ReportSessionService { get; }

        public ModelReporter( IUnityContainer unityContainer )
        {
            ReportAccountService = unityContainer.Resolve< IAccountService >();
            ReportTestService = unityContainer.Resolve< ITestEditorService >();
            ReportSessionService = unityContainer.Resolve< ITestSessionService >();
        }

        public void GenerateReport()
        {
            ReportService( ReportAccountService, "Accounts" );
            ReportService( ReportTestService, "Tests" );
            ReportService( ReportSessionService, "Test Session" );
        }

        private void ReportService < T > ( IDomainEntityService < T > service, string message )
        {
            IList < Guid > ids = service.ViewAll();
            if ( ids.Count == 0 )
                return;

            WriteBodrder( message, '-' );
            WriteLine();

            foreach ( Guid id in ids )
            {
                T dto = service.View( id );
                if ( dto is AccountDto )
                    Report( dto as AccountDto );
                else if ( dto is TestDto )
                    Report( dto as TestDto );
                else if ( dto is TestSessionDto )
                    Report( dto as TestSessionDto );
                WriteLine();
            }

            WriteLine();
        }

        private void Report( AccountDto account )
        {
            WriteLine( $"Guid: {account.DomainId}" );
            WriteLine( $"Login: {account.Login}" );
            WriteLine( $"Role: {account.Role}" );
        }

        private void Report( TestDto test )
        {
            WriteLine( $"Guid: {test.DomainId}" );
            WriteLine( $"Test title: {test.Title}" );
            WriteLine( $"CreatorId: {test.CreatorId}" );
            WriteLine( "Description:" );
            ++Level;
            WriteLine( test.Description.Replace( "\n", $"\n{Indent}" ) );
            --Level;
            WriteLine( $"Count of tests: {test.CountOfTests}" );
            WriteLine( $"Count generate: {test.CountGenerate}" );
            WriteLine( $"Max mark: {test.MaxMark}" );
            WriteLine( "Question groups:" );
            WriteLine();
            ++Level;
            foreach ( QuestionGroupDto questionGroup in test.QuestionGroups )
                Report( questionGroup );
            --Level;
            WriteLine();
        }

        private void Report( QuestionGroupDto questionGroup )
        {
            WriteLine( $"Guid: {questionGroup.DomainId}" );
            WriteLine( $"Mark of one question: {questionGroup.MarkOfOneQuestion}" );
            WriteLine( $"Max mark: {questionGroup.MaxMark}" );
            WriteLine( "Question:" );
            ++Level;
            foreach ( QuestionDto question in questionGroup.Questions )
            {
                WriteLine( $"Guid: {question.DomainId}" );
                WriteLine( $"Text: {question.Text}" );
                if ( question.Options.Count > 0 )
                {
                    WriteLine( "Options:" );
                    ++Level;
                    int num = 1;
                    foreach ( OptionDto option in question.Options )
                    {
                        WriteLine( $"{num}. {option.Text}" );
                        ++num;
                    }
                    --Level;
                }
                WriteLine();
            }
            --Level;
            WriteLine();
        }
        
        private void Report( TestSessionDto session )
        {
            WriteLine( $"Guid: {session.DomainId}" );
            WriteLine( $"Test title: {ReportTestService.View( session.TestId ).Title}" );
            WriteLine( $"Account: {ReportAccountService.View( session.AccountId ).Login}" );
            WriteLine( $"Mark: {session.Mark}" );
            WriteLine( $"Max mark: {session.MaxMark}" );
            WriteLine( $"Is finished: { (session.IsFinished ? "true" : "false") }" );
            WriteLine( "Questions:" );
            WriteLine();
            ++Level;
            foreach ( Guid sessionQuestionId in session.Questions )
            {
                TestSessionQuestionDto sessionQuestion = ReportSessionService.ViewQuestion( sessionQuestionId );
                WriteLine( $"Guid: {sessionQuestion.DomainId}" );
                WriteLine( $"Mark: {sessionQuestion.Mark}" );
                WriteLine( $"Max mark: {sessionQuestion.MaxMark}" );
                WriteLine( sessionQuestion.Question.Text );
                int i = 1;
                ++Level;
                foreach ( var option in sessionQuestion.Question.Options )
                {
                    string text = option.Text;
                    bool isAnswer = sessionQuestion.Answer.Contains( option.Text );
                    WriteLine( $"{i}.{option.Text} { ( isAnswer ? "<-" : "" ) }" );
                    ++i;
                }
                --Level;
                WriteLine();
            }
            --Level;
        }
    }
}
