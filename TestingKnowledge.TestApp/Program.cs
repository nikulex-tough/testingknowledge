﻿using TestingKnowledge.Dependencies;
using TestingKnowledge.Repository.EntityFramework;

using Microsoft.Practices.Unity;

using System;

namespace TestingKnowledge.TestApp
{
    class Program
    {
        static void Main( string [] args )
        {
            try
            {
                using ( var dbContext = new TestingKnowledgeDbContext() )
                using ( var unityContainer = new UnityContainer() )
                {
                    dbContext.Database.Initialize( true );

                    ContainerBoostraper.RegisterTypes( unityContainer, dbContext );

                    TestModelGenerator generator = new TestModelGenerator( unityContainer );
                    generator.GenerateTestData();
                }

                using ( var dbContext = new TestingKnowledgeDbContext() )
                using ( var unityContainer = new UnityContainer() )
                {
                    ContainerBoostraper.RegisterTypes( unityContainer, dbContext );

                    ModelReporter reportGenerator = new ModelReporter( unityContainer );
                    reportGenerator.GenerateReport();
                }
            }
            catch ( Exception e )
            {
                while ( e != null )
                {
                    Console.WriteLine( $"{ e.GetType().FullName}: {e.Message}" );
                    //Console.WriteLine( e.StackTrace );

                    e = e.InnerException;
                }
            }
        }
    }
}
