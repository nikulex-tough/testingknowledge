﻿using Microsoft.Practices.Unity;
using NUnit.Framework;

using TestingKnowledge.Dependencies;
using TestingKnowledge.Repository.Memory;

namespace TestingKnowledge.Service.Tests
{
    public class ServiceFixture
    {
        [SetUp]
        public void FixtureSetUp()
        {
            _container = new UnityContainer();
            _dbContext = new TestingKnowledgeDbContext();
            ContainerBoostraper.RegisterTypesTest( _container, _dbContext );
        }

        [TearDown]
        public void FixtureTearDown()
        {
            _container.Dispose();
            _container = null;
            _dbContext = null;
            _accountService = null;
            _testEditorService = null;
            _testSessionService = null;
            _testSessionManagementService = null;
        }

        private UnityContainer _container;
        private TestingKnowledgeDbContext _dbContext;

        private T LazyLoadService< T >( ref T _service )
        {
            if ( _service == null )
                _service = _container.Resolve< T >();
            return _service;
        }

        private IAccountService _accountService;
        protected IAccountService AccountService => LazyLoadService( ref _accountService );

        private ITestEditorService _testEditorService;
        protected ITestEditorService TestEditorService => LazyLoadService( ref _testEditorService );

        private ITestSessionService _testSessionService;
        protected ITestSessionService TestSessionService => LazyLoadService( ref _testSessionService );

        private ITestSessionManagementService _testSessionManagementService;
        protected ITestSessionManagementService TestSessionManagementService => LazyLoadService( ref _testSessionManagementService );
    }
}
