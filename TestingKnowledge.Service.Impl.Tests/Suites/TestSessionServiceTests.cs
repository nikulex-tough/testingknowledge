﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

using TestingKnowledge.Dto;
using TestingKnowledge.Exceptions;

namespace TestingKnowledge.Service.Tests
{
    [TestFixture]
    public class TestSessionServiceTests : ServiceFixture
    {
        private Guid _creatorId, _userId, _testId;

        [SetUp]
        public void SetUp()
        {
            _creatorId = AccountService.Create( AccountRole.Moderator, "creator", "creator" );
            _testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            _userId = AccountService.Create( AccountRole.User, "user", "user" );
        }

        private Guid CreateTestSession()
        {
            return TestSessionService.CreateTestSession( _userId, _testId );
        }

        private TestSessionDto CreateTestSessionDto()
        {
            return TestSessionService.View( CreateTestSession() );
        }

        [Test]
        public void GenerateEmptyTest()
        {
            TestSessionDto session = CreateTestSessionDto();
            Assert.AreEqual( session.TestId, _testId );
            Assert.AreEqual( session.AccountId, _userId );
            Assert.AreEqual( session.Mark, 0 );
            Assert.AreEqual( session.IsFinished, false );
            Assert.AreEqual( session.MaxMark, 0 );
            Assert.AreEqual( session.Questions.Count, 0 );
        }

        private Guid InitQuestionInput( string answer, float mark )
        {
            Guid questionGroupId = TestEditorService.CreateQuestionGroup( _testId, mark );
            Guid questionInputId = TestEditorService.CreateQuestionInput( questionGroupId, "Text", answer );
            TestEditorService.EditQuestionGroupCountGenerateToMax( questionGroupId );
            return questionInputId;
        }

        [Test]
        public void GenerateTest()
        {
            Guid inputQuestion = InitQuestionInput( "Answer", 5 );

            TestSessionDto session = CreateTestSessionDto();
            Assert.AreEqual( session.Mark, 0 );
            Assert.AreEqual( session.IsFinished, false );
            Assert.AreEqual( session.MaxMark, 5 );
            Assert.AreEqual( session.Questions.Count, 1 );

            TestSessionQuestionDto sessionQuestion = TestSessionService.FindQuestion( session.DomainId, inputQuestion );
            Assert.AreEqual( sessionQuestion.Question.DomainId, inputQuestion );
            Assert.AreEqual( sessionQuestion.SessionId, session.DomainId );
            Assert.AreEqual( sessionQuestion.Mark, 0 );
            Assert.AreEqual( sessionQuestion.MaxMark, 5 );
            Assert.AreEqual( TestSessionService.GetRightAnswer( sessionQuestion.DomainId ).First(), "Answer" );

            TestSessionService.AnswerInput( session.DomainId, inputQuestion, "Answer" );
            Assert.AreEqual( TestSessionService.View( session.DomainId ).Mark, 5 );
            Assert.AreEqual( TestSessionService.FindQuestion( session.DomainId, inputQuestion ).Mark, 5 );
        }

        [Test]
        public void GenerateTest_TypeError()
        {
            Guid inputQuestion = InitQuestionInput( "Answer", 5 );

            Guid sessionId = CreateTestSession();

            Assert.Throws< ServiceUnresolvedEntityException >(() =>
            {
                TestSessionService.AnswerCheckState( sessionId, inputQuestion, Guid.NewGuid(), false );
            } );
        }

        [Test]
        public void FinishTest()
        {
            Guid inputQuestion = InitQuestionInput( "Answer", 5 );
            Guid sessionId = CreateTestSession();
            Assert.AreEqual( TestSessionService.View( sessionId ).IsFinished, false );

            TestSessionService.FinishTestSession( sessionId );
            Assert.AreEqual( TestSessionService.View( sessionId ).IsFinished, true );
        }

        [Test]
        public void AnswerThenFinishTest()
        {
            Guid inputQuestion = InitQuestionInput( "Answer", 5 );
            Guid sessionId = CreateTestSession();
            
            TestSessionService.AnswerInput( sessionId, inputQuestion, "Answer" );
            Assert.AreEqual( TestSessionService.View( sessionId ).IsFinished, false );

            TestSessionService.FinishTestSession( sessionId );
            Assert.AreEqual( TestSessionService.View( sessionId ).IsFinished, true );
        }

        [Test]
        public void ViewAll()
        {
            InitQuestionInput( "Answer", 1 );
            InitQuestionInput( "Answer", 3 );
            InitQuestionInput( "Answer", 5 );

            Guid sessionId1 = CreateTestSession();
            Guid sessionId2 = CreateTestSession();
            Guid sessionId3 = CreateTestSession();

            var all = TestSessionService.ViewAll();
            Assert.AreEqual( all.Count, 3 );
            Assert.IsTrue( all.Contains( sessionId1 ) );
            Assert.IsTrue( all.Contains( sessionId2 ) );
            Assert.IsTrue( all.Contains( sessionId3 ) );
        }

        private Guid InitQuestionSelect( float mark )
        {
            Guid questionGroupId = TestEditorService.CreateQuestionGroup( _testId, mark );
            Guid questionSelectId = TestEditorService.CreateQuestionSelect( questionGroupId, "Text" );
            TestEditorService.EditQuestionGroupCountGenerateToMax( questionGroupId );
            return questionSelectId;
        }

        [Test]
        public void AnswerQuestionSelect()
        {
            Guid questionSelectId = InitQuestionSelect( 5 );
            Guid optionId1 = TestEditorService.CreateQustionOption( questionSelectId, "Option 1", true );
            Guid optionId2 = TestEditorService.CreateQustionOption( questionSelectId, "Option 2", true );
            Guid optionId3 = TestEditorService.CreateQustionOption( questionSelectId, "Option 3", false );
            Guid optionId4 = TestEditorService.CreateQustionOption( questionSelectId, "Option 4", false );
            
            Guid sessionId = CreateTestSession();
            
            TestSessionService.AnswerCheckState( sessionId, questionSelectId, optionId2 );
            TestSessionService.AnswerCheckState( sessionId, questionSelectId, optionId3 );
            Assert.AreEqual( TestSessionService.FindQuestion( sessionId, questionSelectId ).Mark, 2.5 );
            Assert.AreEqual( TestSessionService.View( sessionId ).Mark, 2.5f );
        }

        [Test]
        public void FindByAccount()
        {
            Guid otherTest = TestEditorService.CreateTest( _creatorId, "OtherTest", "OtherTest" );

            Guid user1Session = TestSessionService.CreateTestSession( _userId, _testId );
            Guid user2Session = TestSessionService.CreateTestSession( _creatorId, otherTest );

            Assert.AreEqual( TestSessionManagementService.FindByAccount( _userId ).First().TestId, _testId );
            Assert.AreEqual( TestSessionManagementService.FindByAccount( _creatorId ).First().TestId, otherTest );
        }

        [Test]
        public void FindByTest()
        {
            CreateTestSession();
            CreateTestSession();
            CreateTestSession();

            var sessions = TestSessionManagementService.FindByTest( _testId );
            Assert.AreEqual( sessions.Count, 3 );
        }

        [Test]
        public void ViewActive()
        {
            Guid sessionId1 = CreateTestSession();
            Guid sessionId2 = CreateTestSession();
            Guid sessionId3 = CreateTestSession();

            TestSessionManagementService.FinishTestSession( sessionId2 );
            
            var sessions = TestSessionManagementService.ViewActive( _testId );
            Assert.AreEqual( sessions.Count, 2 );
        }

        [Test]
        public void ViewFinished()
        {
            Guid sessionId1 = CreateTestSession();
            Guid sessionId2 = CreateTestSession();
            Guid sessionId3 = CreateTestSession();

            TestSessionManagementService.FinishTestSession( sessionId2 );

            var sessions = TestSessionManagementService.ViewFinished( _testId );
            Assert.AreEqual( sessions.Count, 1 );
        }

        [Test]
        public void ViewPassedAndNotPassed()
        {
            const string ANSWER = "answer";

            Guid questionGroupId = TestEditorService.CreateQuestionGroup( _testId, 5 );
            Guid q1 = TestEditorService.CreateQuestionInput( questionGroupId, "Question 1", ANSWER );
            Guid q2 = TestEditorService.CreateQuestionInput( questionGroupId, "Question 2", ANSWER );
            Guid q3 = TestEditorService.CreateQuestionInput( questionGroupId, "Question 3", ANSWER );
            Guid q4 = TestEditorService.CreateQuestionInput( questionGroupId, "Question 4", ANSWER );
            TestEditorService.EditQuestionGroupCountGenerateToMax( questionGroupId );

            Guid s1 = CreateTestSession();
            Guid s2 = CreateTestSession();
            Guid s3 = CreateTestSession();

            TestSessionService.AnswerInput( s1, q1, ANSWER );
            TestSessionService.AnswerInput( s1, q2, ANSWER );
            TestSessionService.AnswerInput( s1, q3, ANSWER );
            TestSessionService.AnswerInput( s1, q4, ANSWER );

            TestSessionService.AnswerInput( s2, q1, ANSWER );
            TestSessionService.AnswerInput( s2, q2, ANSWER );
            TestSessionService.AnswerInput( s2, q3, ANSWER );

            TestSessionService.AnswerInput( s3, q1, ANSWER );

            var passed = TestSessionManagementService.ViewPassed( _testId, 15 );
            Assert.AreEqual( passed.Count, 2 );

            Assert.IsTrue( passed.Contains( s1 ) );
            Assert.IsTrue( passed.Contains( s2 ) );
            Assert.IsFalse( passed.Contains( s3 ) );

            var notPassed = TestSessionManagementService.ViewNotPassed( _testId, 15 );
            Assert.AreEqual( notPassed.Count, 1);
        }

        [Test]
        public void MarkValues()
        {
            const string ANSWER = "answer";

            Guid questionGroupId = TestEditorService.CreateQuestionGroup( _testId, 5 );
            Guid q1 = TestEditorService.CreateQuestionInput( questionGroupId, "Question 1", ANSWER );
            Guid q2 = TestEditorService.CreateQuestionInput( questionGroupId, "Question 2", ANSWER );
            Guid q3 = TestEditorService.CreateQuestionInput( questionGroupId, "Question 3", ANSWER );
            Guid q4 = TestEditorService.CreateQuestionInput( questionGroupId, "Question 4", ANSWER );
            TestEditorService.EditQuestionGroupCountGenerateToMax( questionGroupId );

            Guid s1 = CreateTestSession();
            Guid s2 = CreateTestSession();
            Guid s3 = CreateTestSession();

            TestSessionService.AnswerInput( s1, q1, ANSWER );
            TestSessionService.AnswerInput( s1, q2, ANSWER );
            TestSessionService.AnswerInput( s1, q3, ANSWER );
            TestSessionService.AnswerInput( s1, q4, ANSWER );

            TestSessionService.AnswerInput( s2, q1, ANSWER );
            TestSessionService.AnswerInput( s2, q2, ANSWER );
            TestSessionService.AnswerInput( s2, q3, ANSWER );

            TestSessionService.AnswerInput( s3, q1, ANSWER );
            TestSessionService.AnswerInput( s3, q2, ANSWER );

            Assert.AreEqual( TestSessionManagementService.GetMaxMark( _testId ), 20 );
            Assert.AreEqual( TestSessionManagementService.GetMinMark( _testId ), 10 );
            Assert.AreEqual( TestSessionManagementService.GetAverageMark( _testId ), 15 );

        }

        [Test]
        public void DeleteTestSession()
        {
            Guid sessionId1 = CreateTestSession();
            Guid sessionId2 = CreateTestSession();
            Guid sessionId3 = CreateTestSession();

            Assert.AreEqual( TestSessionManagementService.ViewAll().Count, 3 );

            TestSessionManagementService.DeleteTestSession( sessionId3 );
            Assert.AreEqual( TestSessionManagementService.ViewAll().Count, 2 );

            TestSessionManagementService.DeleteTestSession( sessionId1 );
            Assert.AreEqual( TestSessionManagementService.ViewAll().Count, 1 );
        }
    }
}
