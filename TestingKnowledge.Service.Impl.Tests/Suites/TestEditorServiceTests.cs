﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

using TestingKnowledge.Dto;
using TestingKnowledge.Exceptions;

namespace TestingKnowledge.Service.Tests
{
    [TestFixture]
    public class TestEditorServiceTests : ServiceFixture
    {
        private Guid _creatorId;

        [SetUp]
        public void SetUp()
        {
            _creatorId = AccountService.Create( AccountRole.Moderator, "creator", "creator" );
        }

        [TearDown]
        public void TearDown()
        {
            _creatorId = Guid.Empty;
        }
        

        [Test]
        public void CreateTest()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );

            TestDto testDto = TestEditorService.View( testId );
            Assert.NotNull( testDto );
            Assert.AreEqual( testDto.DomainId, testId );
            Assert.AreEqual( testDto.CreatorId, _creatorId );
            Assert.AreEqual( testDto.Title, "MyTest" );
            Assert.AreEqual( testDto.Description, "MyDescription" );
            Assert.AreEqual( testDto.CountGenerate, 0 );
            Assert.AreEqual( testDto.MaxMark, 0 );
            Assert.NotNull( testDto.QuestionGroups );
            Assert.AreEqual( testDto.QuestionGroups.Count, 0 );
        }

        [Test]
        public void CreateTest_NonexistentCreator()
        {
            Assert.Throws< ServiceUnresolvedEntityException >(() =>
            {
                TestEditorService.CreateTest( Guid.NewGuid(), "MyTest", "MyDescription" );
            } );
        }

        [Test]
        public void CreateTest_EmptyTitle()
        {
            Assert.Throws< ServiceValidationException >( () =>
            {
                TestEditorService.CreateTest( _creatorId, "", "MyDescription" );
            } );
        }

        [Test]
        public void CreateTest_EmptyDescription()
        {
            Assert.DoesNotThrow(() =>
            {
                TestEditorService.CreateTest( _creatorId, "MyTest", "" );
            } );
        }

        [Test]
        public void DeleteTest()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Assert.NotNull( TestEditorService.View( testId ) );

            TestEditorService.DeleteTest( testId );
            Assert.Throws< ServiceUnresolvedEntityException >(() =>
            {
                TestEditorService.View( testId );
            });
        }

        [Test]
        public void DeleteTest_TryDeleteNonexistentTest()
        {
            Assert.Throws< ServiceUnresolvedEntityException >(() =>
            {
                TestEditorService.DeleteTest( Guid.NewGuid() );
            } );
        }

        [Test]
        public void ViewTestByCreator()
        {
            Guid creator1TestId1 = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Guid creator1TestId2 = TestEditorService.CreateTest( _creatorId, "MyTest2", "MyDescription" );
            Guid creator1TestId3 = TestEditorService.CreateTest( _creatorId, "MyTest3", "MyDescription" );

            Guid otherCreatorId = AccountService.Create( AccountRole.Moderator, "other", "other" );
            Guid creator2TestId1 = TestEditorService.CreateTest( otherCreatorId, "MyTest", "MyDescription" );
            Guid creator2TestId2 = TestEditorService.CreateTest( otherCreatorId, "MyTest2", "MyDescription" );
            Guid creator2TestId3 = TestEditorService.CreateTest( otherCreatorId, "MyTest3", "MyDescription" );

            var creator1Tests = TestEditorService.ViewByCreator( _creatorId );
            var creator2Tests = TestEditorService.ViewByCreator( otherCreatorId );

            Assert.AreEqual( creator1Tests.Count(), 3 );
            Assert.AreEqual( creator2Tests.Count(), 3 );

            Assert.IsTrue( creator1Tests.Contains( creator1TestId1 ) );
            Assert.IsTrue( creator1Tests.Contains( creator1TestId2 ) );
            Assert.IsTrue( creator1Tests.Contains( creator1TestId3 ) );
            Assert.IsFalse( creator1Tests.Contains( creator2TestId1 ) );
            Assert.IsFalse( creator1Tests.Contains( creator2TestId2 ) );
            Assert.IsFalse( creator1Tests.Contains( creator2TestId3 ) );

            Assert.IsFalse( creator2Tests.Contains( creator1TestId1 ) );
            Assert.IsFalse( creator2Tests.Contains( creator1TestId2 ) );
            Assert.IsFalse( creator2Tests.Contains( creator1TestId3 ) );
            Assert.IsTrue( creator2Tests.Contains( creator2TestId1 ) );
            Assert.IsTrue( creator2Tests.Contains( creator2TestId2 ) );
            Assert.IsTrue( creator2Tests.Contains( creator2TestId3 ) );
        }

        [Test]
        public void ViewTestByCreator_Empty()
        {
            var tests = TestEditorService.ViewByCreator( _creatorId );
            Assert.NotNull( tests );
            Assert.AreEqual( tests.Count(), 0 );
        }

        [Test]
        public void ViewTestByCreator_NonexistentCreator()
        {
            Assert.Throws< ServiceUnresolvedEntityException >( () =>
            {
                TestEditorService.ViewByCreator( Guid.NewGuid() );
            } );
        }

        [Test]
        public void ViewByTitle()
        {
            TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Assert.NotNull( TestEditorService.ViewByTitle( "MyTest" ) );
        }

        [Test]
        public void ViewByTitle_NotFound()
        {
            Assert.Null( TestEditorService.ViewByTitle( "MyTest" ) );
        }

        [Test]
        public void EditTestTitle()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Assert.AreEqual( TestEditorService.View( testId ).Title, "MyTest" );

            TestEditorService.EditTestTitle( testId, "MySuperTest" );
            Assert.AreEqual( TestEditorService.View( testId ).Title, "MySuperTest" );
        }

        [Test]
        public void EditTestTitle_Empty()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );

            Assert.Throws< ServiceValidationException >(() =>
            {
                TestEditorService.EditTestTitle( testId, "" );
            } );
        }

        [Test]
        public void EditTestTitle_NonexistentTest()
        {
            Assert.Throws< ServiceUnresolvedEntityException >( () =>
            {
                TestEditorService.EditTestTitle( Guid.NewGuid(), "MySuperTest" );
            } );
        }

        [Test]
        public void EditTestDescription()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Assert.AreEqual( TestEditorService.View( testId ).Description, "MyDescription" );

            TestEditorService.EditTestDescription( testId, "MySuperDescription" );
            Assert.AreEqual( TestEditorService.View( testId ).Description, "MySuperDescription" );
        }

        [Test]
        public void EditTestDescription_Empty()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Assert.AreEqual( TestEditorService.View( testId ).Description, "MyDescription" );

            TestEditorService.EditTestDescription( testId, "" );
            Assert.AreEqual( TestEditorService.View( testId ).Description, "" );
        }

        [Test]
        public void EditTestDescription_NonexistentTest()
        {
            Assert.Throws< ServiceUnresolvedEntityException >( () =>
            {
                TestEditorService.EditTestDescription( Guid.NewGuid(), "MySuperDescription" );
            } );
        }

        [Test]
        public void CreateQuestionGroup()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Guid questionGroupId = TestEditorService.CreateQuestionGroup( testId, 5 );

            QuestionGroupDto questionGroupDto = TestEditorService.ViewQuestionGroup( questionGroupId );
            Assert.NotNull( questionGroupDto );
            Assert.AreEqual( questionGroupDto.DomainId, questionGroupId );
            Assert.AreEqual( questionGroupDto.MarkOfOneQuestion, 5 );
            Assert.AreEqual( questionGroupDto.MaxMark, 0 );
            Assert.AreEqual( questionGroupDto.CountGenerate, 0 );
            Assert.NotNull( questionGroupDto.Questions );
            Assert.AreEqual( questionGroupDto.Questions.Count, 0 );
        }

        [Test]
        public void CreateQuestionGroup_ErrorMark()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );

            Assert.Throws< ServiceValidationException >(() =>
            {
                TestEditorService.CreateQuestionGroup( testId, -5 );
            } );

            Assert.Throws< ServiceValidationException >( () =>
            {
                TestEditorService.CreateQuestionGroup( testId, 0 );
            } );
        }

        [Test]
        public void DeleteQuestionGroup()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Guid questionGroupId = TestEditorService.CreateQuestionGroup( testId, 5 );
            Assert.NotNull( TestEditorService.ViewQuestionGroup( questionGroupId ) );

            TestEditorService.DeleteQuestionGroup( questionGroupId );
            Assert.Throws< ServiceUnresolvedEntityException >(() =>
            {
                TestEditorService.ViewQuestionGroup( questionGroupId );
            } );
            Assert.Throws< ServiceUnresolvedEntityException >(() =>
            {
                TestEditorService.ViewQuestionGroup( questionGroupId );
            } );
            Assert.AreEqual( TestEditorService.View( testId ).QuestionGroups.Count, 0 );
        }

        [Test]
        public void DeleteTestWithQuestionGroup()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Guid questionGroupId = TestEditorService.CreateQuestionGroup( testId, 5 );
            Assert.NotNull( TestEditorService.ViewQuestionGroup( questionGroupId ) );

            TestEditorService.DeleteTest( testId );
            Assert.Throws< ServiceUnresolvedEntityException >( () =>
            {
                TestEditorService.View( testId );
            } );
            Assert.Throws< ServiceUnresolvedEntityException >( () =>
            {
                TestEditorService.ViewQuestionGroup( questionGroupId );
            } );
        }

        [Test]
        public void DeleteUnexistentQuestionGroup()
        {
            Assert.Throws< ServiceUnresolvedEntityException >( () =>
            {
                TestEditorService.ViewQuestionGroup( Guid.NewGuid() );
            } );
        }

        [Test]
        public void EditQuestionGroup_MarkOfOneQuestion()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Guid questionGroupId = TestEditorService.CreateQuestionGroup( testId, 5 );
            Assert.AreEqual( TestEditorService.ViewQuestionGroup( questionGroupId ).MarkOfOneQuestion, 5 );

            TestEditorService.EditQuestionGroupMarkOfOneQuestion( questionGroupId, 10 );
            Assert.AreEqual( TestEditorService.ViewQuestionGroup( questionGroupId ).MarkOfOneQuestion, 10 );
        }

        [Test]
        public void EditQuestionGroup_MarkOfOneQuestion_Invalid()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Guid questionGroupId = TestEditorService.CreateQuestionGroup( testId, 5 );

            Assert.Throws< ServiceValidationException >(() =>
            {
                TestEditorService.EditQuestionGroupMarkOfOneQuestion( questionGroupId, -5 ); 
            });

            Assert.Throws< ServiceValidationException >(() =>
            {
                TestEditorService.EditQuestionGroupMarkOfOneQuestion( questionGroupId, 0 );
            });
        }

        
        [Test]
        public void CreateQuestionInput()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Guid questionGroupId = TestEditorService.CreateQuestionGroup( testId, 1 );
            Guid questionId = TestEditorService.CreateQuestionInput( questionGroupId, "Text", "RightAnswer" );

            QuestionDto questionDto = TestEditorService.ViewQuestion( questionId );
            Assert.NotNull( questionDto );
            Assert.AreEqual( questionDto.Text, "Text" );
            Assert.NotNull( questionDto.Options );
            Assert.AreEqual( questionDto.Options.Count, 0 );
            Assert.AreEqual( TestEditorService.GetQuestionInputRightAnswer( questionDto.DomainId ), "RightAnswer" );
        }

        [Test]
        public void CreateQuestionSelect()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Guid questionGroupId = TestEditorService.CreateQuestionGroup( testId, 1 );
            Guid questionId = TestEditorService.CreateQuestionSelect( questionGroupId, "Text" );
            Guid optionId1 = TestEditorService.CreateQustionOption( questionId, "Option 1", true );
            Guid optionId2 = TestEditorService.CreateQustionOption( questionId, "Option 2", false );
            Guid optionId3 = TestEditorService.CreateQustionOption( questionId, "Option 3", false );
            Guid optionId4 = TestEditorService.CreateQustionOption( questionId, "Option 4", false );

            QuestionDto questionDto = TestEditorService.ViewQuestion( questionId );
            Assert.NotNull( questionDto );
            Assert.AreEqual( questionDto.Text, "Text" );
            Assert.NotNull( questionDto.Options );

            Assert.AreEqual( questionDto.Options.Count, 4 );
            Assert.AreEqual( TestEditorService.ViewOption( optionId1 ).Text, "Option 1" );
            Assert.AreEqual( TestEditorService.ViewOption( optionId2 ).Text, "Option 2" );
            Assert.AreEqual( TestEditorService.ViewOption( optionId3 ).Text, "Option 3" );
            Assert.AreEqual( TestEditorService.ViewOption( optionId4 ).Text, "Option 4" );
        }

        [Test]
        public void DeleteQuestion()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Guid questionGroupId = TestEditorService.CreateQuestionGroup( testId, 1 );
            Guid questionId = TestEditorService.CreateQuestionInput( questionGroupId, "Text", "RightAnswer" );
            Assert.NotNull( TestEditorService.ViewQuestion( questionId ) );

            TestEditorService.DeleteQuestion( questionId );
            Assert.Throws< ServiceUnresolvedEntityException >( () =>
            {
                TestEditorService.ViewQuestion( questionId );
            } );
        }

        [Test]
        public void EditQuestionText()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Guid questionGroupId = TestEditorService.CreateQuestionGroup( testId, 1 );
            Guid questionId = TestEditorService.CreateQuestionInput( questionGroupId, "Text", "RightAnswer" );
            Assert.AreEqual( TestEditorService.ViewQuestion( questionId ).Text, "Text" );

            TestEditorService.EditQuestionText( questionId, "MyText" );
            Assert.AreEqual( TestEditorService.ViewQuestion( questionId ).Text, "MyText" );
            Assert.AreEqual( TestEditorService.GetQuestionInputRightAnswer( questionId ), "RightAnswer" );
        }

        [Test]
        public void EditQuestionInputRightAnwer()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Guid questionGroupId = TestEditorService.CreateQuestionGroup( testId, 1 );
            Guid questionId = TestEditorService.CreateQuestionInput( questionGroupId, "Text", "RightAnswer" );
            Assert.AreEqual( TestEditorService.GetQuestionInputRightAnswer( questionId ), "RightAnswer" );

            TestEditorService.EditQuestionInputRightAnswer( questionId, "MyRightAnswer" );
            Assert.AreEqual( TestEditorService.GetQuestionInputRightAnswer( questionId ), "MyRightAnswer" );
        }

        [Test]
        public void EditQuestionInputRightAnwer_TypeError()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Guid questionGroupId = TestEditorService.CreateQuestionGroup( testId, 1 );
            Guid questionId = TestEditorService.CreateQuestionSelect( questionGroupId, "Text" );

            Assert.Throws< QuestionTypeException >(() =>
            {
                TestEditorService.EditQuestionInputRightAnswer( questionId, "MyRightAnswer" );
            });
        }

        [Test]
        public void EditQuestionSelect()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Guid questionGroupId = TestEditorService.CreateQuestionGroup( testId, 1 );
            Guid questionId = TestEditorService.CreateQuestionSelect( questionGroupId, "Text" );
            Guid optionId1 = TestEditorService.CreateQustionOption( questionId, "Option 1", true );
            Guid optionId2 = TestEditorService.CreateQustionOption( questionId, "Option 2", true );
            Guid optionId3 = TestEditorService.CreateQustionOption( questionId, "Option 3", false );
            Guid optionId4 = TestEditorService.CreateQustionOption( questionId, "Option 4", false );

            Assert.IsTrue(  TestEditorService.IsRightOption( optionId1 ) );
            Assert.IsTrue(  TestEditorService.IsRightOption( optionId2 ) );
            Assert.IsFalse( TestEditorService.IsRightOption( optionId3 ) );
            Assert.IsFalse( TestEditorService.IsRightOption( optionId4 ) );

            TestEditorService.EditOption( optionId1, "ChangedOption", false );
            Assert.AreEqual( TestEditorService.ViewOption( optionId1 ).Text, "ChangedOption" );
            Assert.IsFalse( TestEditorService.IsRightOption( optionId1 ) );
        }

        [Test]
        public void DeleteOption()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Guid questionGroupId = TestEditorService.CreateQuestionGroup( testId, 1 );
            Guid questionId = TestEditorService.CreateQuestionSelect( questionGroupId, "Text" );
            Guid optionId1 = TestEditorService.CreateQustionOption( questionId, "Option 1", true );
            Guid optionId2 = TestEditorService.CreateQustionOption( questionId, "Option 2", true );
            Guid optionId3 = TestEditorService.CreateQustionOption( questionId, "Option 3", false );
            Guid optionId4 = TestEditorService.CreateQustionOption( questionId, "Option 4", false );
            Assert.AreEqual( TestEditorService.ViewQuestion( questionId ).Options.Count, 4 );

            TestEditorService.DeletOption( optionId1 );
            TestEditorService.DeletOption( optionId3 );

            Assert.AreEqual( TestEditorService.ViewQuestion( questionId ).Options.Count, 2 );
        }

        [Test]
        public void DeleteQuestionWithOption()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Guid questionGroupId = TestEditorService.CreateQuestionGroup( testId, 1 );
            Guid questionId = TestEditorService.CreateQuestionSelect( questionGroupId, "Text" );
            Guid optionId1 = TestEditorService.CreateQustionOption( questionId, "Option 1", true );
            Guid optionId2 = TestEditorService.CreateQustionOption( questionId, "Option 2", true );
            Guid optionId3 = TestEditorService.CreateQustionOption( questionId, "Option 3", false );
            Guid optionId4 = TestEditorService.CreateQustionOption( questionId, "Option 4", false );

            TestEditorService.DeleteQuestion( questionId );
            
            Assert.Throws< ServiceUnresolvedEntityException >(() =>
            {
                TestEditorService.ViewQuestion( questionId );
            } );
            Assert.Throws< ServiceUnresolvedEntityException >(() =>
            {
                TestEditorService.ViewOption( optionId4 );
            } );
        }

        [Test]
        public void CountGenerate()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Guid questionGroupId = TestEditorService.CreateQuestionGroup( testId, 5 );
            TestEditorService.CreateQuestionInput( questionGroupId, "Question 1", "answer" );
            TestEditorService.CreateQuestionInput( questionGroupId, "Question 2", "answer" );
            TestEditorService.CreateQuestionInput( questionGroupId, "Question 3", "answer" );
            TestEditorService.CreateQuestionInput( questionGroupId, "Question 4", "answer" );

            QuestionGroupDto quesitoGroupDto = TestEditorService.ViewQuestionGroup( questionGroupId );
            Assert.AreEqual( quesitoGroupDto.CountGenerate, 0 );
            Assert.AreEqual( quesitoGroupDto.Questions.Count, 4 );
            Assert.AreEqual( quesitoGroupDto.MaxMark, 0 );

            TestEditorService.EditQuestionGroupCountGenerate( questionGroupId, 3 );
            quesitoGroupDto = TestEditorService.ViewQuestionGroup( questionGroupId );
            Assert.AreEqual( quesitoGroupDto.CountGenerate, 3 );
            Assert.AreEqual( quesitoGroupDto.Questions.Count, 4 );
            Assert.AreEqual( quesitoGroupDto.MaxMark, 15 );
        }

        [Test]
        public void CountGenerate_Invalid()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Guid questionGroupId = TestEditorService.CreateQuestionGroup( testId, 1 );
            TestEditorService.CreateQuestionInput( questionGroupId, "Question 1", "answer" );
            TestEditorService.CreateQuestionInput( questionGroupId, "Question 2", "answer" );
            TestEditorService.CreateQuestionInput( questionGroupId, "Question 3", "answer" );
            TestEditorService.CreateQuestionInput( questionGroupId, "Question 4", "answer" );
            
            Assert.Throws< ServiceValidationException >( () =>
            {
                TestEditorService.EditQuestionGroupCountGenerate( questionGroupId, -1 );
            } );
            Assert.Throws< CountGenerateException >( () =>
            {
                TestEditorService.EditQuestionGroupCountGenerate( questionGroupId, 10 );
            } );
        }

        [Test]
        public void CountGenerateToMax()
        {
            Guid testId = TestEditorService.CreateTest( _creatorId, "MyTest", "MyDescription" );
            Guid questionGroupId = TestEditorService.CreateQuestionGroup( testId, 5 );
            TestEditorService.CreateQuestionInput( questionGroupId, "Question 1", "answer" );
            TestEditorService.CreateQuestionInput( questionGroupId, "Question 2", "answer" );
            TestEditorService.CreateQuestionInput( questionGroupId, "Question 3", "answer" );
            TestEditorService.CreateQuestionInput( questionGroupId, "Question 4", "answer" );

            QuestionGroupDto quesitoGroupDto = TestEditorService.ViewQuestionGroup( questionGroupId );
            Assert.AreEqual( quesitoGroupDto.CountGenerate, 0 );
            Assert.AreEqual( quesitoGroupDto.Questions.Count, 4 );
            Assert.AreEqual( quesitoGroupDto.MaxMark, 0 );

            TestEditorService.EditQuestionGroupCountGenerateToMax( questionGroupId );
            quesitoGroupDto = TestEditorService.ViewQuestionGroup( questionGroupId );
            Assert.AreEqual( quesitoGroupDto.CountGenerate, 4 );
            Assert.AreEqual( quesitoGroupDto.Questions.Count, 4 );
            Assert.AreEqual( quesitoGroupDto.MaxMark, 20 );
        }
    }
}