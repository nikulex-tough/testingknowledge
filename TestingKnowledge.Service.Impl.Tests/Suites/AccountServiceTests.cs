﻿using System;
using NUnit.Framework;

using TestingKnowledge.Dto;
using TestingKnowledge.Exceptions;

namespace TestingKnowledge.Service.Tests
{
    [TestFixture]
    public class AccountServiceTests : ServiceFixture
    {
        [Test]
        public void CreateAccount()
        {
            Guid accountId = AccountService.Create( AccountRole.Admin, "admin", "admin" );
            AccountDto accountDto = AccountService.View( accountId );

            Assert.NotNull( accountDto );
            Assert.AreEqual( accountDto.DomainId, accountId );
            Assert.AreEqual( accountDto.Role, AccountRole.Admin );
            Assert.AreEqual( accountDto.Login, "admin" );
            Assert.AreEqual( accountDto.Password, "admin" );
        }

        [Test]
        public void CreateAccount_Duplicate()
        {
            AccountService.Create( AccountRole.Admin, "admin", "admin" );
            
            Assert.Throws< DuplicateNamedEntityException >(() =>
            {
                AccountService.Create( AccountRole.Admin, "admin", "admin" );
            });
        }

        [Test]
        public void CreateAccount_EmptyProperty()
        {
            Assert.Throws< ServiceValidationException >(() =>
            {
                AccountService.Create( AccountRole.Admin, "admin", "" );
            } );
        }

        [Test]
        public void Identify()
        {
            Assert.Null( AccountService.Identify( "admin", "admin" ) );

            Guid accountId = AccountService.Create( AccountRole.Admin, "admin", "admin" );

            AccountDto accountDto = AccountService.Identify( "admin", "admin" );
            Assert.NotNull( accountDto );
            Assert.AreEqual( accountDto.DomainId, accountId );
            Assert.AreEqual( accountDto.Role, AccountRole.Admin );
            Assert.AreEqual( accountDto.Login, "admin" );
            Assert.AreEqual( accountDto.Password, "admin" );
        }

        [Test]
        public void Identify_Error()
        {
            AccountService.Create( AccountRole.Admin, "admin", "admin" );
            Assert.Null( AccountService.Identify( "admin2", "admin" ) );
        }

        [Test]
        public void Identify_PasswordIsNotValid()
        {
            AccountService.Create( AccountRole.Admin, "admin", "admin" );
            Assert.Null( AccountService.Identify( "admin", "password" ) );
        }

        [Test]
        public void DeleteAccount()
        {
            Guid accountId = AccountService.Create( AccountRole.User, "user", "user" );
            Assert.NotNull( AccountService.View( accountId ) );

            AccountService.Delete( accountId );

            Assert.Throws< ServiceUnresolvedEntityException >(() =>
            {
                AccountService.View( accountId );
            } );
        }

        [Test]
        public void DeleteAccount_Error()
        {
            Assert.Throws< ServiceUnresolvedEntityException >( () =>
            {
                AccountService.Delete( Guid.NewGuid() );
            } );
        }

        [Test]
        public void EditLogin()
        {
            Guid accountId = AccountService.Create( AccountRole.User, "VasyaPupkin", "password" );
            Assert.AreEqual( AccountService.View( accountId ).Login, "VasyaPupkin" );

            AccountService.EditLogin( accountId, "VasiliyPupkin" );
            Assert.AreEqual( AccountService.View( accountId ).Login, "VasiliyPupkin" );
        }

        [Test]
        public void EditLogin_Duplicate()
        {
            Guid accountId = AccountService.Create( AccountRole.User, "VasyaPupkin", "password" );
            AccountService.Create( AccountRole.User, "VasyaPupkin2", "password" );

            Assert.AreEqual( AccountService.ViewAll().Count, 2 );
            Assert.Throws< DuplicateNamedEntityException >(() =>
            {
                AccountService.EditLogin( accountId, "VasyaPupkin2" );
            });
        }

        [Test]
        public void EditLogin_EmptyProperty()
        {
            Guid accountId = AccountService.Create( AccountRole.User, "VasyaPupkin", "password" );
            Assert.AreEqual( AccountService.View( accountId ).Login, "VasyaPupkin" );

            Assert.Throws< ServiceValidationException >(() =>
            {
                AccountService.EditLogin( accountId, "" );
            });
        }

        [Test]
        public void EditPassword()
        {
            Guid accountId = AccountService.Create( AccountRole.User, "VasyaPupkin", "password" );
            Assert.AreEqual( AccountService.View( accountId ).Password, "password" );

            AccountService.EditPassword( accountId, "password-changed" );
            Assert.AreEqual( AccountService.View( accountId ).Password, "password-changed" );
        }

        [Test]
        public void EditPassword_EmptyProperty()
        {
            Guid accountId = AccountService.Create( AccountRole.User, "VasyaPupkin", "password" );
            Assert.AreEqual( AccountService.View( accountId ).Login, "VasyaPupkin" );

            Assert.Throws< ServiceValidationException >( () =>
            {
                AccountService.EditPassword( accountId, "" );
            } );
        }

        [Test]
        public void EditAccountRole()
        {
            Guid accountId = AccountService.Create( AccountRole.User, "VasyaPupkin", "password" );
            Assert.AreEqual( AccountService.View( accountId ).Role, AccountRole.User );

            AccountService.EditRole( accountId, AccountRole.Moderator );
            Assert.AreEqual( AccountService.View( accountId ).Role, AccountRole.Moderator );
        }

        [Test]
        public void SelectAllAccounts()
        {
            Guid accountId1 = AccountService.Create( AccountRole.User, "Account1", "password" );
            Guid accountId2 = AccountService.Create( AccountRole.User, "Account2", "password" );
            Guid accountId3 = AccountService.Create( AccountRole.User, "Account3", "password" );
            Guid accountId4 = AccountService.Create( AccountRole.User, "Account4", "password" );
            
            var all = AccountService.ViewAll();
            Assert.IsTrue( all.Contains( accountId1 ) );
            Assert.IsTrue( all.Contains( accountId2 ) );
            Assert.IsTrue( all.Contains( accountId3 ) );
            Assert.IsTrue( all.Contains( accountId4 ) );
        }
    }
}
