﻿using System;

using TestingKnowledge.Model;

namespace TestingKnowledge.Model.Tests
{
    public class TestCreatorFixture
    {
        public Test CreateTest()
        {
            Account creator = new AccountModerator( Guid.NewGuid(), "creator", "creator" );
            Test test = new Test( Guid.NewGuid(), creator, "title", "description" );

            QuestionGroup group1 = new QuestionGroup( Guid.NewGuid(), 1 );
            group1.Add( new QuestionInput( Guid.NewGuid(), "input", "right" ) );
            group1.Add( new QuestionInput( Guid.NewGuid(), "input2", "right2" ) );

            QuestionSelect select = new QuestionSelect( Guid.NewGuid(), "select" );
            select.Add( new Option( Guid.NewGuid(), "option1", true ) );
            select.Add( new Option( Guid.NewGuid(), "option2", false ) );
            select.Add( new Option( Guid.NewGuid(), "option3", true ) );
            select.Add( new Option( Guid.NewGuid(), "option4", false ) );
            group1.Add( select );
            group1.CountGenerate = 3;
            test.Add( group1 );


            QuestionGroup group3 = new QuestionGroup( Guid.NewGuid(), 3 );
            group3.Add( new QuestionInput( Guid.NewGuid(), "input", "right" ) );
            group3.Add( new QuestionInput( Guid.NewGuid(), "input2", "right2" ) );
            group3.CountGenerate = 2;
            test.Add( group3 );


            QuestionGroup group5 = new QuestionGroup( Guid.NewGuid(), 5 );
            group5.Add( new QuestionInput( Guid.NewGuid(), "input", "right" ) );
            group5.Add( new QuestionInput( Guid.NewGuid(), "input2", "right2" ) );
            group5.Add( new QuestionInput( Guid.NewGuid(), "input3", "right3" ) );
            group5.Add( new QuestionInput( Guid.NewGuid(), "input4", "right4" ) );
            group5.CountGenerate = 3;
            test.Add( group5 );

            return test;
        }
    }
}
