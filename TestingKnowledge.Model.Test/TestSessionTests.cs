﻿using NUnit.Framework;
using System;
using System.Linq;

namespace TestingKnowledge.Model.Tests
{
    [TestFixture]
    public class TestSessionTests : TestCreatorFixture
    {
        private static Account Account { get; }

        static TestSessionTests()
        {
            Account = new AccountUser( Guid.NewGuid(), "user", "user" );
        }

        private Test Test { get; set; }

        [SetUp]
        public void SetUp()
        {
            Test = CreateTest();
        }

        [TearDown]
        public void TearDown()
        {
            Test = null;
        }

        [Test]
        public void DefaultValue_TestSession()
        {
            TestSession session = new TestSession();

            Assert.Null( session.Account );
            Assert.Null( session.Test );
            Assert.NotNull( session.SessionQuestions );
            Assert.AreEqual( session.SessionQuestions.Count, 0 );
            Assert.AreEqual( session.MaxMark, 0 );
            Assert.IsFalse( session.IsFinished );
        }

        [Test]
        public void DefaultValue_TestSessionQuestion()
        {
            TestSessionQuestion question = new TestSessionQuestion();

            Assert.Null( question.Question );
            Assert.Null( question.Answer );
            Assert.AreEqual( question.MaxMark, 0.0f );
            Assert.AreEqual( question.RightPercent, 0.0f );
            Assert.AreEqual( question.Mark, 0.0f );
        }

        [Test]
        public void GenerateSession_DoNothing()
        {
            TestSession session = new TestSession( Guid.NewGuid(), Test, Account );

            Assert.IsFalse( session.IsFinished );
            Assert.AreEqual( session.Mark, 0 );
        }
        
        [Test]
        public void GenerateSession_PassTest()
        {
            Assert.DoesNotThrow(() =>
            {
                TestSession session = new TestSession( Guid.NewGuid(), Test, Account );

                Assert.IsFalse( session.IsFinished );
                Assert.AreEqual( session.Mark, 0 );

                foreach ( TestSessionQuestion sessionQuestion in session.SessionQuestions )
                {
                    if ( sessionQuestion.Question is QuestionInput )
                        sessionQuestion.SetAnswer( "right" );
                    else
                    {
                        QuestionSelect select = sessionQuestion.Question as QuestionSelect;
                        foreach ( Option option in select.Options )
                        {
                            if ( option.IsRight )
                                sessionQuestion.SetAnswer( option );
                        }
                    }
                }

                session.IsFinished = true;
            } );
        }
    }
}
