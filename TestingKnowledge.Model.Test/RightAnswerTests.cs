﻿using NUnit.Framework;
using System;
using TestingKnowledge.Exceptions;

namespace TestingKnowledge.Model.Tests
{
    [TestFixture]
    public class RightAnswerTests
    {
        [Test]
        public void AnswerInput_Right()
        {
            Question question = new QuestionInput( Guid.NewGuid(), "My Question", "right" );

            QuestionGroup group = new QuestionGroup( Guid.NewGuid(), 6 );
            group.Add( question );

            TestSessionQuestion sessionQuestion = new TestSessionQuestion( Guid.NewGuid(), question );
            sessionQuestion.SetAnswer( "right" );

            Assert.AreEqual( sessionQuestion.RightPercent, 1 );
            Assert.AreEqual( sessionQuestion.Mark, 6 );
            Assert.AreEqual( sessionQuestion.MaxMark, 6 );
        }

        [Test]
        public void AnswerInput_NotRight()
        {
            Question question = new QuestionInput( Guid.NewGuid(), "My Question", "right" );

            QuestionGroup group = new QuestionGroup( Guid.NewGuid(), 6 );
            group.Add( question );

            TestSessionQuestion sessionQuestion = new TestSessionQuestion( Guid.NewGuid(), question );
            sessionQuestion.SetAnswer( "not right" );

            Assert.AreEqual( sessionQuestion.RightPercent, 0 );
            Assert.AreEqual( sessionQuestion.Mark, 0 );
            Assert.AreEqual( sessionQuestion.MaxMark, 6 );
        }

        [Test]
        public void AnswerInput_TypeException()
        {
            Assert.Throws< QuestionTypeException >(() =>
            {
                Question question = new QuestionSelect( Guid.NewGuid(), "text" );

                QuestionGroup group = new QuestionGroup( Guid.NewGuid(), 6 );
                group.Add( question );

                TestSessionQuestion sessionQuestion = new TestSessionQuestion( Guid.NewGuid(), question );
                sessionQuestion.SetAnswer( "right" );
            } );
        }

        [Test]
        public void AnswerSelect_AbsolutelyRight()
        {
            QuestionSelect question = new QuestionSelect( Guid.NewGuid(), "text" );
            
            Option option1 = new Option( Guid.NewGuid(), "option 1", true );
            Option option2 = new Option( Guid.NewGuid(), "option 2", true );
            Option option3 = new Option( Guid.NewGuid(), "option 3", true );
            Option option4 = new Option( Guid.NewGuid(), "option 4", false );

            question.Add( option1 );
            question.Add( option2 );
            question.Add( option3 );
            question.Add( option4 );

            QuestionGroup group = new QuestionGroup( Guid.NewGuid(), 6 );
            group.Add( question );

            TestSessionQuestion sessionQuestion = new TestSessionQuestion( Guid.NewGuid(), question );
            sessionQuestion.SetAnswer( option1 );
            sessionQuestion.SetAnswer( option2 );
            sessionQuestion.SetAnswer( option3 );
            
            sessionQuestion.SetAnswer( option4 );
            sessionQuestion.SetAnswer( option4, false );

            Assert.AreEqual( sessionQuestion.RightPercent, 1 );
            Assert.AreEqual( sessionQuestion.Mark, 6 );
        }

        [Test]
        public void AnswerSelect_AbsolutelyNotRight()
        {
            QuestionSelect question = new QuestionSelect( Guid.NewGuid(), "text" );

            Option option1 = new Option( Guid.NewGuid(), "option 1", true );
            Option option2 = new Option( Guid.NewGuid(), "option 2", true );
            Option option3 = new Option( Guid.NewGuid(), "option 3", false );
            Option option4 = new Option( Guid.NewGuid(), "option 4", false );

            question.Add( option1 );
            question.Add( option2 );
            question.Add( option3 );
            question.Add( option4 );

            QuestionGroup group = new QuestionGroup( Guid.NewGuid(), 6 );
            group.Add( question );

            TestSessionQuestion sessionQuestion = new TestSessionQuestion( Guid.NewGuid(), question );
            sessionQuestion.SetAnswer( option3 );
            sessionQuestion.SetAnswer( option4 );

            Assert.AreEqual( sessionQuestion.RightPercent, 0 );
            Assert.AreEqual( sessionQuestion.Mark, 0 );

            var asCollection = sessionQuestion.AnswerAsColleciton;
            Assert.AreEqual( asCollection.Count, 2 );
        }

        [Test]
        public void AnswerSelect_Partially_1_From_2()
        {
            QuestionSelect question = new QuestionSelect( Guid.NewGuid(), "text" );

            Option option1 = new Option( Guid.NewGuid(), "option 1", true );
            Option option2 = new Option( Guid.NewGuid(), "option 2", true );
            Option option3 = new Option( Guid.NewGuid(), "option 3", false );
            Option option4 = new Option( Guid.NewGuid(), "option 4", false );

            question.Add( option1 );
            question.Add( option2 );
            question.Add( option3 );
            question.Add( option4 );

            QuestionGroup group = new QuestionGroup( Guid.NewGuid(), 6 );
            group.Add( question );

            TestSessionQuestion sessionQuestion = new TestSessionQuestion( Guid.NewGuid(), question );
            sessionQuestion.SetAnswer( option2 );
            sessionQuestion.SetAnswer( option3 );

            Assert.AreEqual( sessionQuestion.RightPercent, 0.5f );
            Assert.AreEqual( sessionQuestion.Mark, 3 );
        }

        [Test]
        public void AnswerSelect_Partially_2_From_3()
        {
            QuestionSelect question = new QuestionSelect( Guid.NewGuid(), "text" );

            Option option1 = new Option( Guid.NewGuid(), "option 1", true );
            Option option2 = new Option( Guid.NewGuid(), "option 2", true );
            Option option3 = new Option( Guid.NewGuid(), "option 3", true );
            Option option4 = new Option( Guid.NewGuid(), "option 4", false );

            question.Add( option1 );
            question.Add( option2 );
            question.Add( option3 );
            question.Add( option4 );

            QuestionGroup group = new QuestionGroup( Guid.NewGuid(), 6 );
            group.Add( question );

            TestSessionQuestion sessionQuestion = new TestSessionQuestion( Guid.NewGuid(), question );
            sessionQuestion.SetAnswer( option1 );
            sessionQuestion.SetAnswer( option2 );
            sessionQuestion.SetAnswer( option3 );
            sessionQuestion.SetAnswer( option4 );

            Assert.AreEqual( sessionQuestion.RightPercent, 0.75f );
            Assert.AreEqual( sessionQuestion.Mark, 4.5f );
        }

        [Test]
        public void AnswerSelect_Partially_1_From_3()
        {
            QuestionSelect question = new QuestionSelect( Guid.NewGuid(), "text" );

            Option option1 = new Option( Guid.NewGuid(), "option 1", true );
            Option option2 = new Option( Guid.NewGuid(), "option 2", true );
            Option option3 = new Option( Guid.NewGuid(), "option 3", false );
            Option option4 = new Option( Guid.NewGuid(), "option 4", false );

            question.Add( option1 );
            question.Add( option2 );
            question.Add( option3 );
            question.Add( option4 );

            QuestionGroup group = new QuestionGroup( Guid.NewGuid(), 6 );
            group.Add( question );

            TestSessionQuestion sessionQuestion = new TestSessionQuestion( Guid.NewGuid(), question );
            sessionQuestion.SetAnswer( option2 );
            sessionQuestion.SetAnswer( option3 );
            sessionQuestion.SetAnswer( option4 );

            Assert.AreEqual( sessionQuestion.RightPercent, 0.25f );
            Assert.AreEqual( sessionQuestion.Mark, 1.5f );
        }

        [Test]
        public void AnswerSelect_Partially_1_From_5()
        {
            QuestionSelect question = new QuestionSelect( Guid.NewGuid(), "text" );

            Option option1 = new Option( Guid.NewGuid(), "option 1", true );
            Option option2 = new Option( Guid.NewGuid(), "option 2", false );
            Option option3 = new Option( Guid.NewGuid(), "option 3", false );
            Option option4 = new Option( Guid.NewGuid(), "option 4", false );
            Option option5 = new Option( Guid.NewGuid(), "option 5", false );

            question.Add( option1 );
            question.Add( option2 );
            question.Add( option3 );
            question.Add( option4 );
            question.Add( option5 );

            QuestionGroup group = new QuestionGroup( Guid.NewGuid(), 5 );
            group.Add( question );

            TestSessionQuestion sessionQuestion = new TestSessionQuestion( Guid.NewGuid(), question );
            sessionQuestion.SetAnswer( option1 );
            sessionQuestion.SetAnswer( option2 );
            sessionQuestion.SetAnswer( option3 );
            sessionQuestion.SetAnswer( option4 );
            sessionQuestion.SetAnswer( option5 );

            Assert.AreEqual( sessionQuestion.RightPercent, 0.2f );
            Assert.AreEqual( sessionQuestion.Mark, 1 );
        }

        [Test]
        public void AnswerSelect_TypeException()
        {
            Assert.Throws< QuestionTypeException >( () =>
            {
                Question question = new QuestionInput( Guid.NewGuid(), "My Question", "right" );

                QuestionGroup group = new QuestionGroup( Guid.NewGuid(), 6 );
                group.Add( question );

                TestSessionQuestion sessionQuestion = new TestSessionQuestion( Guid.NewGuid(), question );
                sessionQuestion.SetAnswer( new Option( Guid.NewGuid(), "right", true ) );
            } );
        }
    }
}
