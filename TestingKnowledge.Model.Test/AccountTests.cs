﻿using NUnit.Framework;
using System;

using TestingKnowledge.Model;

namespace TestingKnowledge.Model.Tests
{
    [TestFixture]
    public class AccountTests
    {
        [Test]
        public void DefaultValues()
        {
            Account defaultUser = new AccountUser();
            Account defaultModerator = new AccountModerator();
            Account defaultAdmin = new AccountAdmin();

            Assert.AreEqual( defaultUser.DomainId, Guid.Empty );
            Assert.AreEqual( defaultUser.Login, "login" );
            Assert.AreEqual( defaultUser.Password, "password" );
        }

        private class TestVisitor : IAccountVisitor
        {
            private string _resultMessage;

            public static string GetMessage( Account account )
            {
                TestVisitor visitor = new TestVisitor();
                account.Accept( visitor );
                return visitor._resultMessage;
            }

            public void Visit( AccountUser account )
            {
                _resultMessage = "I'm user";
            }

            public void Visit( AccountModerator account )
            {
                _resultMessage = "I'm moderator";
            }

            public void Visit( AccountAdmin account )
            {
                _resultMessage = "I'm admin";
            }
        }

        [Test]
        public void Visitor()
        {
            Account user = new AccountUser( Guid.NewGuid(), "user", "user" );
            Account moderator = new AccountModerator( Guid.NewGuid(), "moderator", "moderator" );
            Account admin = new AccountAdmin( Guid.NewGuid(), "admin", "admin" );

            Assert.AreEqual( TestVisitor.GetMessage( user ), "I'm user" );
            Assert.AreEqual( TestVisitor.GetMessage( moderator ), "I'm moderator" );
            Assert.AreEqual( TestVisitor.GetMessage( admin ), "I'm admin" );
        }

        [Test]
        public void CheckPassword()
        {
            Account account = new AccountUser( Guid.NewGuid(), "user", "password" );
            Assert.IsTrue( account.CheckPassword( "password" ) );
        }
    }
}
