﻿using NUnit.Framework;
using System;
using System.Linq;
using TestingKnowledge.Exceptions;

namespace TestingKnowledge.Model.Tests
{
    [TestFixture]
    public class TestTests : TestCreatorFixture
    {
        [Test]
        public void DefaultValue_Option()
        {
            Option option = new Option();
            
            Assert.AreEqual( option.Text, "option" );
            Assert.IsFalse( option.IsRight );
        }

        [Test]
        public void DefaultValue_QuestionInput()
        {
            QuestionInput input = new QuestionInput();
            
            Assert.AreEqual( input.Text, "text" );
            Assert.Null( input.RightAnswer );
            Assert.Null( input.Group );
        }

        [Test]
        public void DefaultValue_QuestionSelect()
        {
            QuestionSelect select = new QuestionSelect();
            
            Assert.NotNull( select.Options );
            Assert.AreEqual( select.Options.Count, 0 );
            Assert.NotNull( select.RightAnswers );
            Assert.AreEqual( select.RightAnswers.Count, 0 );
        }

        [Test]
        public void DefaultValue_QuestionGroup()
        {
            QuestionGroup group = new QuestionGroup();

            Assert.NotNull( group.Questions );
            Assert.AreEqual( group.Questions.Count, 0 );
            Assert.AreEqual( group.MaxMark, 0 );
            Assert.AreEqual( group.CountGenerate, 0 );
            Assert.AreEqual( group.MarkOfOneQuestion, 0 );
        }

        [Test]
        public void DefaultValue_Test()
        {
            Test test = new Test();

            Assert.Null( test.Creator );
            Assert.NotNull( test.QuestionGroups );
            Assert.AreEqual( test.QuestionGroups.Count, 0 );
            Assert.AreEqual( test.CountOfTests, 0 );
            Assert.AreEqual( test.MaxMark, 0 );
            Assert.AreEqual( test.CountGenerate, 0 );
            Assert.AreEqual( test.Title, "title" );
            Assert.AreEqual( test.Description, "description" );
        }

        [Test]
        public void QuestionSelect_AddOptions_Null()
        {
            Assert.Throws < ArgumentNullException >(() =>
            {
                QuestionSelect select = new QuestionSelect( Guid.NewGuid(), "text" );
                select.Add( null );
            } );
        }

        [Test]
        public void QuestionSelect_AddOptions_AllreadyExist()
        {
            Assert.Throws < ArgumentOutOfRangeException >( () =>
            {
                QuestionSelect select = new QuestionSelect( Guid.NewGuid(), "text" );

                Option option = new Option( Guid.NewGuid(), "option1", true );
                select.Add( option );
                select.Add( option );
            } );
        }

        [Test]
        public void QuestionSelect_AddOptions()
        {
            Assert.DoesNotThrow(() =>
            {
                QuestionSelect select = new QuestionSelect( Guid.NewGuid(), "text" );
                select.Add( new Option( Guid.NewGuid(), "option1", true ) );
                select.Add( new Option( Guid.NewGuid(), "option2", false ) );
                select.Add( new Option( Guid.NewGuid(), "option3", false ) );
                Assert.AreSame( select.Options.First().Question, select );
            } );
        }

        [Test]
        public void QuestionGroup_Add()
        {
            Assert.DoesNotThrow(() =>
            {
                QuestionGroup group = new QuestionGroup( Guid.NewGuid(), 1 );

                Question question = new QuestionInput( Guid.NewGuid(), "test", "right" );
                group.Add( question );

                Assert.NotNull( group.Questions );
                Assert.AreEqual( group.Questions.Count, 1 );
                Assert.AreEqual( group.CountGenerate, 0 );
                Assert.AreSame( question.Group, group );
            } );
        }

        [Test]
        public void QuestionGroup_Add_Null()
        {
            Assert.DoesNotThrow( () =>
            {
                Assert.Throws< ArgumentNullException >( () =>
                {
                    QuestionGroup group = new QuestionGroup( Guid.NewGuid(), 1 );
                    group.Add( null );
                } );
            } );
        }

        [Test]
        public void QuestionGroup_Add_AllreadyExist()
        {
            Assert.DoesNotThrow( () =>
            {
                Assert.Throws< ArgumentOutOfRangeException >(() =>
                {
                    QuestionGroup group = new QuestionGroup( Guid.NewGuid(), 1 );

                    Question question = new QuestionInput( Guid.NewGuid(), "test", "right" );
                    group.Add( question );
                    group.Add( question );
                } );
            } );
        }

        [Test]
        public void QuestionGroup_Compare()
        {
            QuestionGroup group1 = new QuestionGroup( Guid.NewGuid(), 3 );
            QuestionGroup group2 = new QuestionGroup( Guid.NewGuid(), 5 );
            QuestionGroup group3 = new QuestionGroup( Guid.NewGuid(), 5 );

            Assert.AreEqual( group1.CompareTo( group2 ), -1 );
            Assert.AreEqual( group2.CompareTo( group3 ), 0 );
            Assert.AreEqual( group3.CompareTo( group1 ), 1 );
        }

        [Test]
        public void QuestionGroup_Generate_Empty()
        {
            Assert.Throws< InvalidOperationException >(() =>
            {
                QuestionGroup group = new QuestionGroup( Guid.NewGuid(), 3 );
                group.Generate();
            } );
        }

        [Test]
        public void QuestionGroup_Generate_NoSetCountGenerate()
        {
            Assert.Throws< InvalidOperationException >( () =>
            {
                QuestionGroup group = new QuestionGroup( Guid.NewGuid(), 3 );

                Question question = new QuestionInput( Guid.NewGuid(), "test", "right" );
                group.Add( question );

                group.Generate();
            } );
        }

        [Test]
        public void QuestionGroup_Generate_Generate()
        {
            QuestionGroup group = new QuestionGroup( Guid.NewGuid(), 3 );

            group.Add( new QuestionInput( Guid.NewGuid(), "test", "right" ) );
            group.Add( new QuestionInput( Guid.NewGuid(), "test2", "right2" ) );
            group.Add( new QuestionInput( Guid.NewGuid(), "test3", "right3" ) );
            group.CountGenerate = 2;
            Assert.AreEqual( group.MaxMark, 6 );

            var questions = group.Generate();
            Assert.AreEqual( questions.Count, 2 );
            Assert.IsTrue( group.Questions.Contains( questions [ 0 ] ) );
            Assert.IsTrue( group.Questions.Contains( questions [ 1 ] ) );
        }

        [Test]
        public void Test_AddGroup_Null()
        {
            Assert.Throws< ArgumentNullException >(() =>
            {
                Account creator = new AccountModerator( Guid.NewGuid(), "creator", "creator" );
                Test test = new Test( Guid.NewGuid(), creator, "title", "description" );
                test.Add( null );
            } );
        }

        [Test]
        public void Test_AddGroup_AllreadyExist()
        {
            Assert.Throws< ArgumentOutOfRangeException >( () =>
            {
                Account creator = new AccountModerator( Guid.NewGuid(), "creator", "creator" );
                Test test = new Test( Guid.NewGuid(), creator, "title", "description" );

                QuestionGroup group = new QuestionGroup( Guid.NewGuid(), 3 );
                test.Add( group );
                test.Add( group );
            } );
        }
        
        [Test]
        public void Test_AddGroup()
        {
            Test test = CreateTest();

            Assert.AreEqual( test.CountGenerate, 8 );
            Assert.AreEqual( test.CountOfTests, 9 );
            Assert.AreEqual( test.MaxMark, 3 + 6 + 15 );
            Assert.AreSame( test.QuestionGroups.First().Test, test );
        }

        [Test]
        public void Test_Generate()
        {
            Test test = CreateTest();
            var generated = test.Generate();

            Assert.AreEqual( generated.Count, test.CountGenerate );
            foreach ( Question question in generated )
                Assert.IsTrue( test.QuestionGroups.Contains( question.Group ) );
        }
    }
}
