﻿using System.Collections.Generic;
using TestingKnowledge.Model;
using TestingKnowledge.Dto;
using System;

namespace TestingKnowledge.Service.Impl
{
    static class DtoBuilder
    {
        private class AccountRoleVisitor : IAccountVisitor
        {
            private AccountRole _role;

            public static AccountRole Get( Account account )
            {
                AccountRoleVisitor visitor = new AccountRoleVisitor();
                account.Accept( visitor );
                return visitor._role;
            }

            public void Visit( AccountUser account )
            {
                _role = AccountRole.User;
            }

            public void Visit( AccountModerator account )
            {
                _role = AccountRole.Moderator;
            }

            public void Visit( AccountAdmin account )
            {
                _role = AccountRole.Admin;
            }
        }

        public static AccountDto ToDto ( this Account account )
        {
            return new AccountDto(
                    account.DomainId
                ,   account.Login
                ,   account.Password
                ,   AccountRoleVisitor.Get( account )
            );
        }

        #region Test

        public static TestDto ToDto ( this Test test )
        {
            IList < QuestionGroupDto > questionsGroups = new List < QuestionGroupDto > ();
            foreach ( QuestionGroup questionGroup in test.QuestionGroups )
                questionsGroups.Add( questionGroup.ToDto() );

            return new TestDto (
                    test.DomainId
                ,   test.Title
                ,   test.Description
                ,   test.Creator.DomainId
                ,   test.CountOfTests
                ,   test.CountGenerate
                ,   test.MaxMark
                ,   questionsGroups
            );
        }
        
        public static QuestionGroupDto ToDto( this QuestionGroup questionGroup )
        {
            IList < QuestionDto > questions = new List < QuestionDto > ();
            foreach ( Question question in questionGroup.Questions )
                questions.Add( question.ToDto() );

            return new QuestionGroupDto(
                    questionGroup.DomainId
                ,   questionGroup.Test.DomainId
                ,   questions
                ,   questionGroup.MarkOfOneQuestion
                ,   questionGroup.MaxMark
                ,   questionGroup.CountGenerate
            );
        }

        private class QuestionToDtoVisitor : IQuestionVisitor
        {
            private QuestionDto _dto;

            public static QuestionDto Get( Question question )
            {
                QuestionToDtoVisitor visitor = new QuestionToDtoVisitor();
                question.Accept( visitor );
                return visitor._dto;
            }

            public void Visit( QuestionInput question )
            {
                _dto = new QuestionDto(
                        question.DomainId
                    ,   question.Group.DomainId
                    ,   question.Text
                    ,   new List< OptionDto > ()
                );
            }

            public void Visit( QuestionSelect question )
            {
                IList < OptionDto > options = new List < OptionDto > ();
                foreach ( Option option in question.Options )
                    options.Add( option.ToDto() );
                
                _dto = new QuestionDto(
                        question.DomainId
                    ,   question.Group.DomainId
                    ,   question.Text
                    ,   options
                );
            }
        }

        public static QuestionDto ToDto( this Question question )
        {
            return QuestionToDtoVisitor.Get( question );
        }

        public static OptionDto ToDto( this Option option )
        {
            return new OptionDto( option.DomainId, option.Question.DomainId, option.Text );
        }

        #endregion

        #region Test Session

        public static TestSessionDto ToDto( this TestSession testSession )
        {
            IList < Guid > questions = new List < Guid > ();
            foreach ( TestSessionQuestion testSessionQuestion in testSession.SessionQuestions )
                questions.Add( testSessionQuestion.DomainId );

            return new TestSessionDto(
                    testSession.DomainId
                ,   testSession.Test.DomainId
                ,   testSession.Account.DomainId
                ,   testSession.Mark
                ,   testSession.MaxMark
                ,   testSession.IsFinished
                ,   questions
            );
        }

        public static TestSessionQuestionDto ToDto( this TestSessionQuestion testSessionQuestion )
        {
            return new TestSessionQuestionDto(
                    testSessionQuestion.DomainId
                ,   testSessionQuestion.Session.DomainId
                ,   testSessionQuestion.Question.ToDto()
                ,   testSessionQuestion.AnswerAsColleciton
                ,   testSessionQuestion.Mark
                ,   testSessionQuestion.MaxMark
            );
        }

        #endregion
    }
}
