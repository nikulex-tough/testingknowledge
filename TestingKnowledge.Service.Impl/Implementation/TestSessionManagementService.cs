﻿using System;
using System.Linq;
using System.Collections.Generic;

using TestingKnowledge.Model;
using TestingKnowledge.Dto;

namespace TestingKnowledge.Service.Impl
{
    public class TestSessionManagementService : TestSessionCommonService, ITestSessionManagementService
    {
        private IList < TestSessionDto > ToDto( IQueryable < TestSession > sessions )
        {
            IList < TestSessionDto > sessionsDto = new List < TestSessionDto > ();
            foreach ( var session in sessions )
                sessionsDto.Add( session.ToDto() );
            return sessionsDto;
        }

        public IList < TestSessionDto > FindByAccount( Guid accountId )
        {
            return ToDto( TestSessionRepository.FindByAccount( ResolveAccount( accountId ) ) );
        }

        public IList < TestSessionDto > FindByTest( Guid testId )
        {
            return ToDto( TestSessionRepository.FindByTest( ResolveTest( testId ) ) );
        }

        public float GetAverageMark( Guid testId )
        {
            return FindByTest( testId ).Average( ( ts ) => ts.Mark );
        }

        public float GetMaxMark( Guid testId )
        {
            return FindByTest( testId ).Max( ( ts ) => ts.Mark );
        }

        public float GetMinMark( Guid testId )
        {
            return FindByTest( testId ).Min( ( ts ) => ts.Mark );
        }

        public IList < Guid > ViewActive( Guid testId )
        {
            return TestSessionRepository.SelectActiveIds().ToList();
        }

        public IList < Guid > ViewFinished( Guid testId )
        {
            return TestSessionRepository.SelectFinishedIds().ToList();
        }

        public IList < Guid > ViewPassed( Guid testId, float minMark )
        {
            return TestSessionRepository.SelectPassedIds( minMark ).ToList();
        }

        public IList < Guid > ViewNotPassed( Guid testId, float minMark )
        {
            return TestSessionRepository.SelectNotPassedIds( minMark ).ToList();
        }

        public void DeleteTestSession( Guid testSessionId )
        {
            TestSession session = ResolveTestSession( testSessionId );
            TestSessionRepository.Delete( session );
            foreach ( TestSessionQuestion question in session.SessionQuestions )
                TestSessionQuestionRepository.Delete( question );
        }
    }
}
