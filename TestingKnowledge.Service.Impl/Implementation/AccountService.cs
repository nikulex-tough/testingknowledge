﻿using System;
using System.Collections.Generic;
using System.Linq;

using TestingKnowledge.Model;
using TestingKnowledge.Repository;
using TestingKnowledge.Exceptions;
using TestingKnowledge.Dto;

using Microsoft.Practices.Unity;

namespace TestingKnowledge.Service.Impl
{
    public class AccountService : IAccountService
    {
        [Dependency]
        protected IAccountRepository AccountRepository { get; set; }

        private Account ResolveAccount( Guid accountId )
        {
            return ServiceUtils.ResolveEntity( AccountRepository, accountId );
        }

        private void CheckDuplicate( string login )
        {
            CheckDuplicate( AccountRepository.FindByLogin( login ) );
        }

        private void CheckDuplicate( Account account )
        {
            if ( account != null )
                throw new DuplicateNamedEntityException( typeof( Account ), account.Login );
        }


        public IList < Guid > ViewAll ()
        {
            return AccountRepository.SelectAllDomainIds().ToList();
        }
        
        public AccountDto View ( Guid accountId )
        {
            return ResolveAccount( accountId )?.ToDto();
        }


        public AccountDto Identify ( string login, string password )
        {
            Account account = AccountRepository.FindByLogin( login );
            if ( account == null )
                return null;

            if ( !account.CheckPassword( password ) )
                return null;

            return account.ToDto();
        }

        private void Create( Guid id, AccountRole role, string login, string password )
        {
            CheckDuplicate( login );
            switch ( role )
            {
                case AccountRole.Admin:
                    AccountRepository.Add( new AccountAdmin( id, login, password ) );
                    break;

                case AccountRole.Moderator:
                    AccountRepository.Add( new AccountModerator( id, login, password ) );
                    break;

                case AccountRole.User:
                    AccountRepository.Add( new AccountUser( id, login, password ) );
                    break;
            }
        }

        public Guid Create( AccountRole role, string login, string password )
        {
            Guid id = Guid.NewGuid();
            Create( id, role, login, password );
            return id;
        }

        public void Delete( Guid accountId )
        {
            AccountRepository.Delete( ResolveAccount( accountId ) );
        }

        public void EditRole( Guid accountId, AccountRole role )
        {
            Account account = ResolveAccount( accountId );
            AccountRepository.Delete( account );
            AccountRepository.Commit();

            AccountRepository.StartTransaction();
            Create( account.DomainId, role, account.Login, account.Password );
        }

        public void EditLogin( Guid accountId, string login )
        {
            CheckDuplicate( login );
            Account account = ResolveAccount( accountId );
            account.Login = login;
        }

        public void EditPassword( Guid accountId, string newPassword )
        {
            Account account = ResolveAccount( accountId );
            account.Password = newPassword;
        }
    }
}
