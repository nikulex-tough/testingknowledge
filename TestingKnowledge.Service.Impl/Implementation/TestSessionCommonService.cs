﻿using System;
using System.Linq;
using System.Collections.Generic;

using TestingKnowledge.Model;
using TestingKnowledge.Repository;
using TestingKnowledge.Dto;

using Microsoft.Practices.Unity;

namespace TestingKnowledge.Service.Impl
{
    public class TestSessionCommonService : ITestSessionCommonService
    {
        [Dependency]
        protected ITestSessionRepository TestSessionRepository { get; set; }
        
        [Dependency]
        protected ITestSessionQuestionRepository TestSessionQuestionRepository { get; set; }
        
        [Dependency]
        protected ITestRepostory TestRepository { get; set; }

        [Dependency]
        protected IQuestionRepository QuestionRepository { get; set; }

        [Dependency]
        protected IAccountRepository AccountRepository { get; set; }

        #region Resolve

        protected TestSession ResolveTestSession( Guid testSessionId )
        {
            return ServiceUtils.ResolveEntity( TestSessionRepository, testSessionId );
        }

        protected TestSessionQuestion ResolveTestSessionQuestion( Guid testSessionQuestionId )
        {
            return ServiceUtils.ResolveEntity( TestSessionQuestionRepository, testSessionQuestionId );
        }

        protected Account ResolveAccount( Guid accountId )
        {
            return ServiceUtils.ResolveEntity( AccountRepository, accountId );
        }

        protected Test ResolveTest( Guid testId )
        {
            return ServiceUtils.ResolveEntity( TestRepository, testId );
        }

        protected Question ResolveQuestion( Guid questionId )
        {
            return ServiceUtils.ResolveEntity( QuestionRepository, questionId );
        }

        #endregion

        #region Impl

        public TestSessionDto View( Guid domainId )
        {
            return ResolveTestSession( domainId ).ToDto();
        }

        public IList < Guid > ViewAll()
        {
            return TestSessionRepository.SelectAllDomainIds().ToList();
        }

        public Guid CreateTestSession( Guid accountId, Guid testId )
        {
            Guid id = Guid.NewGuid();
            TestSession testSession = new TestSession( id, ResolveTest( testId ), ResolveAccount( accountId ) );

            TestSessionRepository.Add( testSession );
            foreach ( TestSessionQuestion question in testSession.SessionQuestions )
                TestSessionQuestionRepository.Add( question );

            return id;
        }

        public void FinishTestSession( Guid testSessionId )
        {
            ResolveTestSession( testSessionId ).IsFinished = true;
        }

        private class AnswerConverterVisitor : IQuestionVisitor
        {
            private IList < string > _answer = new List<string>();

            public static IList < string > Get( Question question )
            {
                AnswerConverterVisitor visitor = new AnswerConverterVisitor();
                question.Accept( visitor );
                return visitor._answer;
            }

            public void Visit( QuestionInput question )
            {
                _answer.Add( question.RightAnswer );
            }

            public void Visit( QuestionSelect question )
            {
                foreach ( Option option in question.Options )
                {
                    if ( option.IsRight )
                        _answer.Add( option.Text );
                }
            }
        }


        public IList < string > GetRightAnswer( Guid sessionQuestionId )
        {
            return AnswerConverterVisitor.Get( ResolveTestSessionQuestion( sessionQuestionId ).Question );
        }

        public TestSessionQuestionDto ViewQuestion( Guid sessionQuestionId )
        {
            return ResolveTestSessionQuestion( sessionQuestionId ).ToDto();
        }

        public TestSessionQuestionDto FindQuestion( Guid sessionId, Guid questionId )
        {
            return TestSessionQuestionRepository.FindBySessionAndQuestion(
                    ResolveTestSession( sessionId )
                ,   ResolveQuestion( questionId )
            ).ToDto();
        }

        #endregion
    }
}
