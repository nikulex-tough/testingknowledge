﻿using System;
using TestingKnowledge.Model;
using TestingKnowledge.Repository;

using Microsoft.Practices.Unity;

namespace TestingKnowledge.Service.Impl
{
    public class TestSessionService : TestSessionCommonService, ITestSessionService
    {
        [Dependency]
        protected IOptionRepository OptionRepostory { get; set; }
        
        protected Option ResolveOption( Guid optionId )
        {
            return ServiceUtils.ResolveEntity( OptionRepostory, optionId );
        }

        public void AnswerCheckState( Guid sessionId, Guid questionId, Guid optionId, bool selected = true )
        {
            TestSessionQuestionRepository.FindBySessionAndQuestion(
                    ResolveTestSession( sessionId )
                ,   ResolveQuestion( questionId )
            ).SetAnswer( ResolveOption( optionId ), selected );
        }

        public void AnswerInput( Guid sessionId, Guid questionId, string answer )
        {
            TestSessionQuestionRepository.FindBySessionAndQuestion(
                    ResolveTestSession( sessionId )
                ,   ResolveQuestion( questionId )
            ).SetAnswer( answer );
        }
    }
}
