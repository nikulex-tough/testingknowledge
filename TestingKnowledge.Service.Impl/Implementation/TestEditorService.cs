﻿using System;
using System.Linq;
using System.Collections.Generic;

using TestingKnowledge.Model;
using TestingKnowledge.Repository;
using TestingKnowledge.Exceptions;
using TestingKnowledge.Dto;

using Microsoft.Practices.Unity;

namespace TestingKnowledge.Service.Impl
{
    public class TestEditorService : ITestEditorService
    {
        [Dependency]
        public ITestRepostory TestRepository { get; set; }

        [Dependency]
        public IQuestionGroupRepository QuestionGroupRepository { get; set; }

        [Dependency]
        public IQuestionRepository QuestionRepository { get; set; }

        [Dependency]
        public IOptionRepository OptionRepository { get; set; }

        [Dependency]
        public IAccountRepository AccountRepository { get; set; }

        #region Resolve

        private Test ResolveTest( Guid testId )
        {
            return ServiceUtils.ResolveEntity( TestRepository, testId );
        }

        private QuestionGroup ResolveQuestionGroup( Guid questionGroupId )
        {
            return ServiceUtils.ResolveEntity( QuestionGroupRepository, questionGroupId );
        }

        private Question ResolveQuestion( Guid questionId )
        {
            return ServiceUtils.ResolveEntity( QuestionRepository, questionId );
        }

        private Option ResolveOption( Guid OptionId )
        {
            return ServiceUtils.ResolveEntity( OptionRepository, OptionId );
        }

        private Account ResolveAccount( Guid accountId )
        {
            return ServiceUtils.ResolveEntity( AccountRepository, accountId );
        }

        #endregion
        
        #region Impl

        public TestDto View( Guid domainId )
        {
            return ResolveTest( domainId ).ToDto();
        }

        public IList < Guid > ViewAll()
        {
            return TestRepository.SelectAllDomainIds().ToList();
        }
        
        public IQueryable< Guid > ViewByCreator( Guid accountId )
        {
            return TestRepository.FindByCreator( ResolveAccount( accountId ) );
        }

        public TestDto ViewByTitle( string title )
        {
            return TestRepository.FindByTitle( title )?.ToDto();
        }

        public QuestionGroupDto ViewQuestionGroup( Guid questionGroupId )
        {
            return ResolveQuestionGroup( questionGroupId ).ToDto();
        }

        public QuestionDto ViewQuestion( Guid questionId )
        {
            return ResolveQuestion( questionId ).ToDto();
        }

        public OptionDto ViewOption( Guid optionId )
        {
            return ResolveOption( optionId ).ToDto();
        }

        public Guid CreateTest( Guid accountId, string title, string description )
        {
            Account account = ResolveAccount( accountId );
            Guid id = Guid.NewGuid();
            TestRepository.Add( new Test( id, account, title, description ) );
            return id;
        }

        public Guid CreateQuestionGroup( Guid testId, float markOfOneQuestion )
        {
            Test test = ResolveTest( testId );

            Guid id = Guid.NewGuid();
            QuestionGroup newQuestionGroup = new QuestionGroup( id, markOfOneQuestion );
            test.Add( newQuestionGroup );
            QuestionGroupRepository.Add( newQuestionGroup );
            return id;
        }

        public Guid CreateQuestionInput( Guid questionGroupId, string text, string rightAnswer )
        {
            QuestionGroup questionGroup = ResolveQuestionGroup( questionGroupId );

            Guid id = Guid.NewGuid();
            QuestionInput newQuestionInput = new QuestionInput( id, text, rightAnswer );
            questionGroup.Add( newQuestionInput );
            QuestionRepository.Add( newQuestionInput );
            return id;
        }

        public Guid CreateQuestionSelect( Guid questionGroupId, string text )
        {
            QuestionGroup questionGroup = ResolveQuestionGroup( questionGroupId );

            Guid id = Guid.NewGuid();
            QuestionSelect questionSelect = new QuestionSelect( id, text );
            questionGroup.Add( questionSelect );
            QuestionRepository.Add( questionSelect );
            return id;
        }

        public Guid CreateQustionOption( Guid questionSelectId, string text, bool isRight )
        {
            QuestionSelect select = ResolveQuestion( questionSelectId ) as QuestionSelect;
            if ( select == null )
                throw new QuestionTypeException( typeof( QuestionSelect ), typeof( QuestionInput ), questionSelectId );

            Guid id = Guid.NewGuid();
            Option newOption = new Option( id, text, isRight );
            select.Add( newOption );
            OptionRepository.Add( newOption );
            return id;
        }

        private class DeleteQuestionVisitor : IQuestionVisitor
        {
            private TestEditorService service;

            public static void Delete( TestEditorService service, Question question )
            {
                service.QuestionRepository.Delete( question );
                question.Accept( new DeleteQuestionVisitor { service = service } );
                question.Free();
            }

            public void Visit( QuestionSelect question )
            {
                foreach ( Option option in question.Options )
                    service.OptionRepository.Delete( option );
            }

            public void Visit( QuestionInput question )
            {
            }
        }

        private void DeleteQuestionGroup( QuestionGroup questionGroup )
        {
            QuestionGroupRepository.Delete( questionGroup );
            foreach ( Question question in questionGroup.Questions )
                DeleteQuestionVisitor.Delete( this, question );
        }

        public void DeleteTest( Guid testId )
        {
            Test test = ResolveTest( testId );
            TestRepository.Delete( test );

            foreach ( QuestionGroup questionGroup in test.QuestionGroups )
                DeleteQuestionGroup( questionGroup );
        }

        public void DeleteQuestionGroup( Guid questionGroupId )
        {
            QuestionGroup questionGroup = ResolveQuestionGroup( questionGroupId );
            questionGroup.Free();
            DeleteQuestionGroup( questionGroup );
        }

        public void DeleteQuestion( Guid questionId )
        {
            DeleteQuestionVisitor.Delete( this, ResolveQuestion( questionId ) );
        }

        public void DeletOption( Guid optionId )
        {
            Option option = ResolveOption( optionId );
            option.Free();
            OptionRepository.Delete( option );
        }


        public void EditTestTitle( Guid testId, string title )
        {
            ResolveTest( testId ).Title = title;
        }

        public void EditTestDescription( Guid testId, string description )
        {
            ResolveTest( testId ).Description = description;
        }

        public void EditQuestionGroupMarkOfOneQuestion( Guid questionGroupId, float markOfOneQuestion )
        {
            ResolveQuestionGroup( questionGroupId ).MarkOfOneQuestion = markOfOneQuestion;
        }

        public void EditQuestionGroupCountGenerate( Guid questionGroupId, int countGenerate )
        {
            QuestionGroup questionGroup = ResolveQuestionGroup( questionGroupId );
            if ( questionGroup.Questions.Count < countGenerate )
                throw new CountGenerateException( countGenerate, questionGroup.Questions.Count );
            questionGroup.CountGenerate = countGenerate;
        }

        public void EditQuestionGroupCountGenerateToMax( Guid questionGroupId )
        {
            QuestionGroup group = ResolveQuestionGroup( questionGroupId );
            group.CountGenerate = group.Questions.Count;
        }

        public void EditQuestionInputRightAnswer( Guid questionInputId, string rightAnswer )
        {
            QuestionInput input = ResolveQuestion( questionInputId ) as QuestionInput;
            if ( input == null )
                throw new QuestionTypeException( typeof( QuestionInput ), typeof( QuestionSelect ), questionInputId );
            input.RightAnswer = rightAnswer;
        }

        public string GetQuestionInputRightAnswer(Guid questionInputId)
        {
            QuestionInput input = ResolveQuestion( questionInputId ) as QuestionInput;
            if ( input == null )
                throw new QuestionTypeException( typeof( QuestionInput ), typeof( QuestionSelect ), questionInputId );
            return input.RightAnswer;
        }

        public void EditOption( Guid optionId, string text, bool isRight )
        {
            Option option = ResolveOption( optionId );
            option.Text = text;
            option.IsRight = isRight;
        }
        
        public bool IsRightOption( Guid optionId )
        {
            return ResolveOption( optionId ).IsRight;
        }

        public void EditQuestionText( Guid questionId, string text )
        {
            ResolveQuestion( questionId ).Text = text;
        }

        #endregion
    }
}
