﻿using System;
using TestingKnowledge.Repository;
using TestingKnowledge.Exceptions;

namespace TestingKnowledge.Service.Impl
{
    sealed class ServiceUtils
    {
        public static TEntity ResolveEntity < TEntity > ( IRepository < TEntity > repository, Guid domainId )
            where TEntity : Utils.Entity
        {
            TEntity entity = repository.FindByDomainId( domainId );
            if ( entity != null )
                return entity;

            throw new ServiceUnresolvedEntityException( typeof( TEntity ), domainId );
        }
    }
}
