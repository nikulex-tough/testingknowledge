﻿namespace TestingKnowledge.Repository.Memory
{
    public class TestingKnowledgeDbContext : ITransactionDbContext
    {
        public AccountRepository AccountRepository { get; } = new AccountRepository();

        public OptionRepository OptionRepository { get; } = new OptionRepository();
        public QuestionRepository QuestionRepository { get; } = new QuestionRepository();
        public QuestionGroupRepository QuestionGroupRepository { get; } = new QuestionGroupRepository();
        public TestRepository TestRepository { get; } = new TestRepository();

        public TestSessionQuestionRepository TestSessionQuestionRepository { get; } = new TestSessionQuestionRepository();
        public TestSessionRepository TestSessionRepository { get; } = new TestSessionRepository();

        public void BeginTransaction()
        {
            AccountRepository.StartTransaction();
            OptionRepository.StartTransaction();
            QuestionRepository.StartTransaction();
            QuestionGroupRepository.StartTransaction();
            TestRepository.StartTransaction();
            TestSessionQuestionRepository.StartTransaction();
            TestSessionRepository.StartTransaction();
        }

        public void Commit()
        {
            AccountRepository.Commit();
            OptionRepository.Commit();
            QuestionRepository.Commit();
            QuestionGroupRepository.Commit();
            TestRepository.Commit();
            TestSessionQuestionRepository.Commit();
            TestSessionRepository.Commit();
        }

        public void Rollback()
        {
            AccountRepository.Rollback();
            OptionRepository.Rollback();
            QuestionRepository.Rollback();
            QuestionGroupRepository.Rollback();
            TestRepository.Rollback();
            TestSessionQuestionRepository.Rollback();
            TestSessionRepository.Rollback();
        }
    }
}
