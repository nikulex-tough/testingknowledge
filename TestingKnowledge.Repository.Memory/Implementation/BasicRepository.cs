﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace TestingKnowledge.Repository.Memory
{
    public class BasicRepository< T > : IRepository< T >, IDisposable
        where T : Utils.Entity
    {
        private static List < T > _list = new List < T > ();
        private static List < T > _temp;

        protected List < T > Collection
        {
            get
            {
                if ( _temp != null )
                    return _temp;
                return _list;
            }
        }

        public void Dispose()
        {
            _temp = null;
            _list.Clear();
        }

        public void StartTransaction()
        {
            _temp = new List < T > ( _list );
        }

        public void Commit()
        {
            _list = _temp;
            _temp = null;
        }

        public void Rollback()
        {
            _temp = null;
        }

        public void Add( T t )
        {
            Collection.Add( t );
        }

        public int Count()
        {
            return Collection.Count;
        }

        public void Delete( T t )
        {
            Collection.Remove( t );
        }

        public T FindByDomainId( Guid domainId )
        {
            return Collection.SingleOrDefault( e => e.DomainId == domainId );
        }

        public IQueryable < T > LoadAll()
        {
            return Collection.AsQueryable();
        }

        public IQueryable < Guid > SelectAllDomainIds()
        {
            return LoadAll().Select( e => e.DomainId );
        }
    }
}
