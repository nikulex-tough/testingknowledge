﻿using System.Linq;
using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.Memory
{
    public class AccountRepository : BasicRepository< Account >, IAccountRepository
    {
        public Account FindByLogin( string login )
        {
            return Collection.SingleOrDefault( a => a.Login == login );
        }
    }
}