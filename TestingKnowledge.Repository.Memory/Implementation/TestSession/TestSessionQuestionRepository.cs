﻿using System;
using System.Linq;

using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.Memory
{
    public class TestSessionQuestionRepository : BasicRepository< TestSessionQuestion >, ITestSessionQuestionRepository
    {
        public IQueryable< Guid > FindByQuestion( Question question )
        {
            return Collection
                .Where( sq => sq.Question.DomainId == question.DomainId )
                .Select( sq => sq.DomainId )
                .AsQueryable();
        }

        public TestSessionQuestion FindBySessionAndQuestion( TestSession session, Question question )
        {
            return Collection.FirstOrDefault(
                sq => sq.Question.DomainId == question.DomainId
                && sq.Session.DomainId == session.DomainId
            );
        }
    }
}
