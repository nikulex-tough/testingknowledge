﻿using System;
using System.Linq;
using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.Memory
{
    public class TestSessionRepository : BasicRepository < TestSession >, ITestSessionRepository
    {
        public IQueryable < TestSession > FindByAccount( Account account )
        {
           return Collection.Where( ( ts ) => ts.Account == account ).AsQueryable();
        }

        public IQueryable < TestSession > FindByTest( Test test )
        {
            return Collection.Where( ( ts ) => ts.Test == test ).AsQueryable();
        }

        public IQueryable < Guid > SelectActiveIds()
        {
            return Collection.Where( ( ts ) => ts.IsFinished == false ).Select( ( ts ) => ts.DomainId ).AsQueryable();
        }

        public IQueryable< Guid > SelectFinishedIds()
        {
            return Collection.Where( ( ts ) => ts.IsFinished ).Select( ( ts ) => ts.DomainId ).AsQueryable();
        }

        public IQueryable< Guid > SelectPassedIds( float minMark )
        {
            return Collection.Where( ( ts ) => ts.Mark >= minMark ).Select( ( ts ) => ts.DomainId ).AsQueryable();
        }

        public IQueryable< Guid > SelectNotPassedIds( float minMark )
        {
            return Collection.Where( ( ts ) => ts.Mark < minMark ).Select( ( ts ) => ts.DomainId ).AsQueryable();
        }
    }
}
