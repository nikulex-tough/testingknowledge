﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.Memory
{
    public class QuestionRepository : BasicRepository< Question >, IQuestionRepository
    {
    }
}
