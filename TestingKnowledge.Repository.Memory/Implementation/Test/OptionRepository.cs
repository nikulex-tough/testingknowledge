﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.Memory
{
    public class OptionRepository : BasicRepository< Option >, IOptionRepository
    {
    }
}
