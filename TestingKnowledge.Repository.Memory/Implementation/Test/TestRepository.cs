﻿using System;
using System.Linq;
using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.Memory
{
    public class TestRepository : BasicRepository< Test >, ITestRepostory
    {
        public IQueryable< Guid > FindByCreator( Account account )
        {
            return Collection
                .Where( ( t ) => t.Creator.DomainId == account.DomainId )
                .Select( ( t ) => t.DomainId ).AsQueryable();
        }

        public Test FindByTitle( string title )
        {
            return Collection.FirstOrDefault( ( t ) => t.Title == title );
        }
    }
}
