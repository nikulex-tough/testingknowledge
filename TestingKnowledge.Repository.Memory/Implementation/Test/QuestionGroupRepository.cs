﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository.Memory
{
    public class QuestionGroupRepository : BasicRepository< QuestionGroup >, IQuestionGroupRepository
    {
    }
}