﻿function identifyAccount( login, password )
{
    $.post(
       "/Login/Identify",
       {
           login: login,
           password: password
       },
       function (responce)
       {
           if ( responce == null )
               console.log( "Identify error" );

           else if ( responce.domainId == null )
               console.log( responce );

           else
           {
               window.location = "./";
               return;
           }

           $( "#input-login, #input-password" ).addClass( "has-error" );
       }
   ).fail( function () {
       alert( "Identification failed!" );
   });
}

function tryLogin()
{
    var login = $( "#input-login input" ).val();
    var password = $( "#input-password input" ).val();

    identifyAccount( login, password );
}

$( document ).ready( function () {

    $( '#orderDetailsForm' ).bootstrapValidator({
        message: 'This value is not valid',
        live: 'enabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            inputLogin: {
                message: 'The login is not valid',
                validators: {
                    notEmpty: {
                        message: 'The name is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 2,
                        max: 30,
                        message: 'The name must be more than 2 and less than 30 characters long'
                    },
                }
            },
            inputPassword: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and can\'t be empty'
                    }
                }
            },
        }
    });
});