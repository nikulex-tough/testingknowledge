﻿var navSelectedAccount;

function removeSelectedAccount()
{
    if ( navSelectedAccount == null )
        return;

    $.post(
        "/Accounts/RemoveAccount",
        {
            accountId: navSelectedAccount
        },
        function ()
        {
            $.toast( "Account removed", 3000 );
            $( "#account-" + navSelectedAccount ).remove();
            navSelectedAccount = null;
        }
    ).fail(function ()
    {
        alert( "Failed to remove item" );
        navSelectedAccount = null;
    });
}

function onShowEditAccount()
{
    if ( navSelectedAccount == null )
        return;

    $.post(
        "/Accounts/View",
        {
            accountId: navSelectedAccount
        },
        function ( account ) {
            if ( account == null )
                alert( "account is empty" );
            else
            {
                $( "#inputLogin-edit" ).val( account.login );
                $( "#inputPassword-edit" ).val( account.password );
                $( "#selectRole-edit" ).val( account.role );
            }
        }
    ).fail(function () {
        alert( "Failed to show account" );
    });
}

function editSelectedAccount()
{
    if ( navSelectedAccount == null )
        return;

    var accountLogin = $( "#inputLogin-edit" ).val();
    var accountPassword = $( "#inputPassword-edit" ).val();
    var acccountRole = $( "#selectRole-edit" ).val();

    $.post(
        "/Accounts/EditAccount",
        {
            accountId: navSelectedAccount,
            login: accountLogin,
            password: accountPassword,
            role: acccountRole
        },
        function ( responce )
        {
            if ( responce != true )
            {
                alert( responce );
                return;
            }

            $( "#inputLogin-edit" ).val( "" );
            $( "#inputPassword-edit" ).val( "" );
            
            var roleText = $( "#selectRole-edit option[value='" + acccountRole + "']" ).text();

            var accountSelector = "#account-" + navSelectedAccount + " ";
            $( accountSelector + ".account-login" ).text( accountLogin );
            $( accountSelector + ".account-role" ).text( roleText );

            $( "#modal-edit-account" ).modal( 'hide' );
            $.toast( "Account changed", 3000 );

            navSelectedAccount = null;
        }
    ).fail(function () {
        alert( "Failed to edit account" );
        navSelectedAccount = null;
    });
}

function resetCreateForm()
{
    $( "#inputLogin-create" ).val( "" );
    $( "#inputPassword-create" ).val( "" );
    $( "#selectRole-create" ).val( 0 );
}

function createAccount()
{
    var accountLogin = $( "#inputLogin-create" ).val();
    var accountPassword = $( "#inputPassword-create" ).val();
    var acccountRole = $( "#selectRole-create" ).val();

    $.post(
        "/Accounts/CreateAccount",
        {
            login: accountLogin,
            password: accountPassword,
            role: acccountRole
        },
        function ( responce )
        {
            resetCreateForm();

            if ( typeof responce === "object" )
            {
                console.log( responce );
                alert( responce.Message );
                return;
            }

            $( "#accounts-table tbody" ).append( $( responce ) );
            $( "#modal-create-account" ).modal( 'hide' );
            $.toast( "Account created", 3000 );
        }
    ).fail( function () {
        alert( "Failed to create account" );
    });
}