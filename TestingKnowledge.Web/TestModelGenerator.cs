﻿using System;
using System.Collections.Generic;

using TestingKnowledge.Dto;
using TestingKnowledge.Service;
using TestingKnowledge.Service.Impl;
using Microsoft.Practices.Unity;

namespace TestingKnowledge.Web
{
    class TestModelGenerator
    {
        private IUnityContainer UnityContainer { get; }

        public TestModelGenerator( IUnityContainer unityContainer )
        {
            UnityContainer = unityContainer;
        }

        #region Generate

        public void GenerateTestData()
        {
            GenerateAccounts();
            GenerateTests();
            GenerateTestSessions();
        }

        private Guid _vasyaId, _petyaId, _userId, _moderatorId, _adminId; 
        private void GenerateAccounts()
        {
            IAccountService service = UnityContainer.Resolve< IAccountService >();

            _userId = service.Create( AccountRole.User, "user", "user" );
            _moderatorId = service.Create( AccountRole.Moderator, "mod", "mod" );
            _adminId = service.Create( AccountRole.Admin, "admin", "admin" );

            _vasyaId = service.Create( AccountRole.User, "Vasya", "123" );
            _petyaId = service.Create( AccountRole.User, "Petya", "321" );

            Guid testEdit = service.Create( AccountRole.User, "TestEdit", "TestEdit" );
            service.EditLogin( testEdit, "TestEdit-newlogin" );
            service.EditPassword( testEdit, "newpassword" );
            service.EditRole( testEdit, AccountRole.Admin );

            Guid testDelete = service.Create( AccountRole.User, "TestDelet", "password" );
            service.Delete( testDelete );
            
            AccountDto vasya = service.Identify( "Vasya", "123" );
            AccountDto petya = service.Identify( "Petya", "not valid" );
            AccountDto unknown = service.Identify( "Unknown", "not valid" );
        }
        
        private Guid _testId;
        private void GenerateTests()
        {
            ITestEditorService service = UnityContainer.Resolve< ITestEditorService >();
            _testId = service.CreateTest( _moderatorId, "My Test", "My Test Description\nnewline\nbla-bla-bla" );

            Guid questionGroup1 = service.CreateQuestionGroup( _testId, 1 );
            Guid questionGroup2 = service.CreateQuestionGroup( _testId, 2 );
            Guid questionGroup3 = service.CreateQuestionGroup( _testId, 5 );

            service.CreateQuestionInput( questionGroup1, "Course number", "4" );
            service.CreateQuestionInput( questionGroup1, "Our star name", "Sun" );
            service.CreateQuestionInput( questionGroup1, "Our planet is", "Earth" );
            Guid testEditRightAnswerId = service.CreateQuestionInput( questionGroup1, "University is", "KhNURE" );
            service.CreateQuestionInput( questionGroup1, "City is", "Kharkiv" );
            service.CreateQuestionInput( questionGroup1, "The best football club", "Not defined :P" );
            service.CreateQuestionInput( questionGroup1, "Veni vidi", "vici" );

            service.CreateQuestionInput( questionGroup2, "How many spartans is fighting near Phermophilles?", "300" );
            service.CreateQuestionInput( questionGroup2, "In year: ", "480 BC" );
            service.CreateQuestionInput( questionGroup2, "XVII is", "17" );
            service.CreateQuestionInput( questionGroup2, "cos of 60 degrees is", "0.5" );
            service.CreateQuestionInput( questionGroup2, "Formula of tnt is: ", "C7H5N3O6" );
            service.CreateQuestionInput( questionGroup2, "The best time for studying is: ", "7.45" );
            service.CreateQuestionInput( questionGroup2, "The worse time for stydying is", "0.00" );

            service.CreateQuestionInput( questionGroup3, "Your favorite programming language is", "not defined" );
            service.CreateQuestionInput( questionGroup3, "Heavyweight in box", "200 pounds" );
            service.CreateQuestionInput( questionGroup3, "Apollo 11 go to moon in : ", "1969" );
            service.CreateQuestionInput( questionGroup3, "NBA Winner 2016 is", "Cavs" );
            service.CreateQuestionInput( questionGroup3, "Quantum Break weight is ", "67 Gb" );

            Guid selectForFirstGroup1 = service.CreateQuestionSelect( questionGroup1, "How many bits in a byte?" );
            service.CreateQustionOption( selectForFirstGroup1, "1", false );
            service.CreateQustionOption( selectForFirstGroup1, "4", false );
            service.CreateQustionOption( selectForFirstGroup1, "8" , true );
            service.CreateQustionOption( selectForFirstGroup1, "16", false );

            Guid selectForFirstGroup2_1 = service.CreateQuestionSelect( questionGroup2, "What is bigger?" );
            service.CreateQustionOption( selectForFirstGroup2_1, "Human", false );
            service.CreateQustionOption( selectForFirstGroup2_1, "Lion", false );
            service.CreateQustionOption( selectForFirstGroup2_1, "Elephant", true );
            service.CreateQustionOption( selectForFirstGroup2_1, "Shark", false );

            Guid selectForFirstGroup2_2 = service.CreateQuestionSelect( questionGroup2, "What is harder?" );
            Guid testEditOptionId = service.CreateQustionOption( selectForFirstGroup2_2, "Pick up the girl", true );
            service.CreateQustionOption( selectForFirstGroup2_2, "Drive the car", false );
            service.CreateQustionOption( selectForFirstGroup2_2, "Writing the good code", false );
            service.CreateQustionOption( selectForFirstGroup2_2, "To lift up the elephant", true );
            service.CreateQustionOption( selectForFirstGroup2_2, "Run for 100 hrs", false );

            Guid selectoForGroup3 = service.CreateQuestionSelect( questionGroup3, "What is the 4th cosmic velocity (km/s)" );
            service.CreateQustionOption( selectoForGroup3, "100", false );
            service.CreateQustionOption( selectoForGroup3, "150", false );
            service.CreateQustionOption( selectoForGroup3, "200", false );
            service.CreateQustionOption( selectoForGroup3, "300", false );
            service.CreateQustionOption( selectoForGroup3, "400", false );
            service.CreateQustionOption( selectoForGroup3, "450", false );
            Guid testDeleteOptionId = service.CreateQustionOption( selectoForGroup3, "500", false );
            service.CreateQustionOption( selectoForGroup3, "550", true );

            service.EditQuestionGroupCountGenerateToMax( questionGroup1 );
            service.EditQuestionGroupCountGenerateToMax( questionGroup2 );
            service.EditQuestionGroupCountGenerate( questionGroup3, 3 );

            service.EditQuestionGroupMarkOfOneQuestion( questionGroup2, 3 );
            service.EditTestTitle( _testId, "RENAMED TITLE" );
            service.EditTestDescription( _testId, "NEW DESCRIPTION" );
            service.EditQuestionText( selectForFirstGroup2_2, "What is harder????" );
            service.EditOption( testEditOptionId, "To fly", false );
            service.EditQuestionInputRightAnswer( testEditRightAnswerId, "NURE" );
            service.DeletOption( testDeleteOptionId );
        }
        
        private Random _rand = new Random();
        private bool RandPlease => _rand.Next( 0, 2 ) == 0;

        private void GenerateTestSessions()
        {
            ITestSessionService service = UnityContainer.Resolve < ITestSessionService >();

            Guid sessionId = service.CreateTestSession( _vasyaId, _testId );
            TestSessionDto session = service.View( sessionId );
            foreach ( Guid sessionQuestionId in session.Questions )
            {
                TestSessionQuestionDto sessionQuestion = service.ViewQuestion( sessionQuestionId );
                QuestionDto question = sessionQuestion.Question;
                
                if ( question.Options.Count > 0 )
                {
                    foreach ( var option in question.Options )
                        service.AnswerCheckState( sessionId, sessionQuestionId, option.DomainId, RandPlease );
                }
                else
                {
                    service.AnswerInput(
                            sessionId
                        ,   sessionQuestionId
                        ,       RandPlease
                            ?   service.GetRightAnswer( sessionQuestionId )[ 0 ]
                            :   "not right answer"
                    );
                }
            }

            service.FinishTestSession( session.DomainId );
        }

        #endregion
    }
}
