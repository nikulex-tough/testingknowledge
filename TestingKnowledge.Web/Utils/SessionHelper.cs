﻿using System;
using Microsoft.AspNetCore.Http;

namespace TestingKnowledge.Web.Utils
{
    public class SessionHelper
    {
        private ISession session;

        public SessionHelper ( ISession session )
        {
            this.session = session;
        }

        /*
        public Guid? GetCartId ()
        {
            string result = session.GetString( CartId );
            if ( result == null || result.Length == 0 )
                return null;

            return Guid.Parse( result );
        }

        public void SetCartId ( Guid cartId )
        {
            session.SetString( CartId, cartId.ToString() );
        }

        public void ResetCartId ()
        {
            session.Remove( CartId );
        }

        public Guid? GetOrderId ()
        {
            string result = session.GetString( OrderId );
            if ( result == null || result.Length == 0 )
                return null;

            return Guid.Parse( result );
        }

        public void SetOrderId ( Guid orderId )
        {
            session.SetString( OrderId, orderId.ToString() );
        }


        private static readonly string CartId = "CART_ID";
        private static readonly string OrderId = "ORDER_ID";
        */
    }
}
