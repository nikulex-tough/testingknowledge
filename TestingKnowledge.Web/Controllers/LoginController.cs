﻿using System;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Practices.Unity;

using TestingKnowledge.Dto;
using TestingKnowledge.Service;
using Microsoft.AspNetCore.Http;

namespace TestingKnowledge.Web.Controllers
{
    public class LoginController : BasicController
    {
        public IActionResult Index() => View();

        [HttpPost]
        public IActionResult Identify( string login, string password )
        {
            try
            {
                AccountDto account = accountService.Identify( login, password );
                if ( account != null )
                    Response.Cookies.Append(
                            HomeController.ACCESS_TOKEN_COOKIE
                        ,   account.DomainId.ToString()
                        ,   new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears( 1 ) }
                    );
                return Json( account );
            }
            catch ( Exception ex )
            {
                return Json( ex.Message );
            }
        }
    }
}
