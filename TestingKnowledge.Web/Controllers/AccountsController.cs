﻿using System;
using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Practices.Unity;

using TestingKnowledge.Dto;
using TestingKnowledge.Service;
using TestingKnowledge.Web.Models;
using TestingKnowledge.Web.Utils;

namespace TestingKnowledge.Web.Controllers
{
    public class AccountsController : BasicController
    {
        public IActionResult Index()
        {
            IActionResult result = AcceptOrRedirect( AccountRole.Admin );
            if ( result != null )
                return result;

            AccountsModel model = new AccountsModel();
            model.AuthAccount = GetAccount();
            model.Accounts = LoadAccounts();
            return View( model  );
        }

        private IList< AccountSecureDto > LoadAccounts()
        {
            List< AccountSecureDto > accounts = new List< AccountSecureDto >();
            foreach ( Guid accountId in accountService.ViewAll() )
                accounts.Add( accountService.View( accountId ) );
            return accounts;
        }

        [HttpPost]
        public IActionResult View( Guid accountId )
        {
            IActionResult result = AcceptOrRedirect( AccountRole.Admin );
            if ( result != null )
                return null;

            return Json( accountService.View( accountId ) );
        }

        [HttpPost]
        public IActionResult CreateAccount( string login, string password, AccountRole role )
        {
            IActionResult result = AcceptOrRedirect( AccountRole.Admin );
            if ( result != null )
                return null;

            try
            {
                Guid accountId = accountService.Create( role, login, password );

                AccountModel model = new AccountModel();
                model.AuthAccount = GetAccount();
                model.Account = new AccountDto( accountId, login, password, role );
                return View( "Account", model );
            }
            catch ( Exception ex )
            {
                return Json( ex );
            }
        }

        [HttpPost]
        public IActionResult EditAccount( Guid accountId, string login, string password, AccountRole role )
        {
            IActionResult result = AcceptOrRedirect( AccountRole.Admin );
            if ( result != null )
                return null;
            
            try
            {
                AccountDto account = accountService.View( accountId );

                if (  account.Login != login )
                    accountService.EditLogin( accountId, login );
            
                if ( account.Password != password )
                    accountService.EditPassword( accountId, password );
            
                if ( account.Role != role )
                    accountService.EditRole( accountId, role );
            }
            catch( Exception ex )
            {
                return Json( ex.Message );
            }

            return Json( true );
        }

        [HttpPost]
        public void RemoveAccount( Guid accountId )
        {
            IActionResult result = AcceptOrRedirect( AccountRole.Admin );
            if ( result != null )
                return;

            accountService.Delete( accountId );
        }
    }
}
