﻿using System;

using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;

using Microsoft.Practices.Unity;

using TestingKnowledge.Dto;
using TestingKnowledge.Service;
using TestingKnowledge.Web.Models;

namespace TestingKnowledge.Web.Controllers
{
    public class HomeController : BasicController
    {
        public IActionResult Index()
        {
            AccountDto account = GetAccount();
            if ( account != null )
            {
                HomeModel model = new HomeModel();
                model.AuthAccount = account;
                return View( model );
            }

            return new RedirectToActionResult( "Index", "Login", null );
        }

        [HttpPost]
        public void ExitAccount()
        {
            Response.Cookies.Delete( ACCESS_TOKEN_COOKIE );
        }

        [HttpPost]
        public void SetLocale( string language )
        {
            string culture = "en-US";
            if ( language == "ru" )
                culture = "ru-RU";
            else if ( language == "uk" )
                culture = "uk-UA";

            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue( new RequestCulture( culture ) ),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears( 1 ) }
            );
        }


        public IActionResult FatalError()
        {
            var feature = HttpContext.Features.Get< IExceptionHandlerFeature >();
            var error = feature?.Error;

            var errorViewModel = new ErrorViewModel();
            errorViewModel.ErrorTitle = "500 Fatal Server Error";
            errorViewModel.ErrorMessage = error.Message;
            
            return View( "Error", errorViewModel );
        }


        public IActionResult Error404( string errorPath )
        {
            var errorViewModel = new ErrorViewModel();
            errorViewModel.ErrorTitle = "404";
            errorViewModel.ErrorMessage = string.Format( "The requested page {0} was not found", errorPath );

            return View( "Error", errorViewModel );
        }
    }
}
