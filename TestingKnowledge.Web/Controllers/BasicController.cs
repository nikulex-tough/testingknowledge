﻿using System;

using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;

using Microsoft.Practices.Unity;

using TestingKnowledge.Dto;
using TestingKnowledge.Service;
using TestingKnowledge.Web.Models;

namespace TestingKnowledge.Web.Controllers
{
    public abstract class BasicController : Controller
    {
        public const string ACCESS_TOKEN_COOKIE = "access-token";

        protected AccountDto GetAccount()
        {
            string accountCookie;

            if ( Request.Cookies.TryGetValue( ACCESS_TOKEN_COOKIE, out accountCookie ) )
            {
                try
                {
                   return accountService.View( Guid.Parse( accountCookie ) );
                }
                catch
                {
                    Response.Cookies.Delete( ACCESS_TOKEN_COOKIE );
                }
            }

            return null;
        }

        protected IActionResult AcceptOrRedirect( AccountRole role )
        {
            AccountDto account = GetAccount();
            if ( account == null || ( account.Role & role ) == 0  )
                return new RedirectToActionResult( "Index", "Home", null );
            return null;
        }

        [Dependency]
        protected IAccountService accountService { get; set; }
    }
}
