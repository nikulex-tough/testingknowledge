﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Practices.Unity;

using TestingKnowledge.Dto;
using TestingKnowledge.Service;
using TestingKnowledge.Web.Models;
using TestingKnowledge.Web.Utils;

namespace TestingKnowledge.Web.Controllers
{
    public class TestsController : BasicController
    {
        public IActionResult Index()
        {
            IActionResult result = AcceptOrRedirect( AccountRole.Admin | AccountRole.Moderator );
            if ( result != null )
                return result;

            TestsModel model = new TestsModel();
            model.AuthAccount = GetAccount();
            model.Tests = LoadAccountTests();
            return View( model );
        }

        private IList< TestDto > LoadAccountTests()
        {
            AccountDto account = GetAccount();
            if ( account == null )
                return new List< TestDto > ();

            var tests = testEditorService.ViewByCreator( account.DomainId ).ToArray();
            return tests.Select( testId => testEditorService.View( testId ) ).ToList();
        }

        [Dependency]
        protected ITestEditorService testEditorService { get; set; }
    }
}
