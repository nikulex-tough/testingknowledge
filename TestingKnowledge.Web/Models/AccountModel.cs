﻿using TestingKnowledge.Dto;

namespace TestingKnowledge.Web.Models
{
    public class AccountModel : BasicModel
    {
        public AccountDto Account { get; set; }
    }
}
