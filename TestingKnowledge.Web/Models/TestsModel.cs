﻿using System.Collections.Generic;
using TestingKnowledge.Dto;

namespace TestingKnowledge.Web.Models
{
    public class TestsModel : BasicModel
    {
        public IList< TestDto > Tests { get; set; }
    }
}
