﻿using TestingKnowledge.Dto;

namespace TestingKnowledge.Web.Models
{
    public abstract class BasicModel
    {
        public AccountDto AuthAccount { get; set; }
    }
}
