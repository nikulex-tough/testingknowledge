﻿using System.Collections.Generic;
using TestingKnowledge.Dto;

namespace TestingKnowledge.Web.Models
{
    public class AccountsModel : BasicModel
    {
        public IList< AccountSecureDto > Accounts { get; set; }
    }
}
