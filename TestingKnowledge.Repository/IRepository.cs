﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Linq;

namespace TestingKnowledge.Repository
{
    public interface IRepository< T > where T : Utils.Entity
    {
        void StartTransaction ();

        void Commit ();

        void Rollback ();

        int Count ();

        IQueryable< T > LoadAll ();

        void Add ( T t );

        void Delete ( T t );

        T FindByDomainId ( Guid domainId );
        
        IQueryable< Guid > SelectAllDomainIds ();
    }
}
