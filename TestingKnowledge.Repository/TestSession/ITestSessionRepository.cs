﻿using System;
using System.Linq;
using TestingKnowledge.Model;

namespace TestingKnowledge.Repository
{
    public interface ITestSessionRepository : IRepository < TestSession >
    {
        IQueryable < TestSession > FindByAccount( Account account );
        
        IQueryable < TestSession > FindByTest( Test test );

        IQueryable < Guid > SelectActiveIds();

        IQueryable < Guid > SelectFinishedIds();

        IQueryable < Guid > SelectPassedIds( float minMark );

        IQueryable < Guid > SelectNotPassedIds( float minMark );
    }
}
