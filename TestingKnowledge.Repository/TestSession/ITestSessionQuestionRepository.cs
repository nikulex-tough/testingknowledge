﻿using System;
using System.Linq;
using TestingKnowledge.Model;

namespace TestingKnowledge.Repository
{
    public interface ITestSessionQuestionRepository : IRepository < TestSessionQuestion >
    {
        IQueryable< Guid > FindByQuestion( Question question );

        TestSessionQuestion FindBySessionAndQuestion( TestSession session, Question question );
    }
}
