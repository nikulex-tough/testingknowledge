﻿namespace TestingKnowledge.Repository
{
    public interface ITransactionDbContext
    {
        void BeginTransaction();
        void Commit();
        void Rollback();
    }
}