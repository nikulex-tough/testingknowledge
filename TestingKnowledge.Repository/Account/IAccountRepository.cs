﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository
{
    public interface IAccountRepository : IRepository < Account >
    {
        Account FindByLogin( string login );
    }
}
