﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository
{
    public interface IOptionRepository : IRepository < Option >
    {
    }
}