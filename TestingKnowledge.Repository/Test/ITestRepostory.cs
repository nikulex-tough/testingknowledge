﻿using System;
using System.Linq;
using TestingKnowledge.Model;

namespace TestingKnowledge.Repository
{
    public interface ITestRepostory : IRepository < Test >
    {
        IQueryable < Guid > FindByCreator( Account account );

        Test FindByTitle( string title );
    }
}
