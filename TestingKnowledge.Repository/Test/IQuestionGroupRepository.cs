﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository
{
    public interface IQuestionGroupRepository : IRepository < QuestionGroup >
    {
    }
}
