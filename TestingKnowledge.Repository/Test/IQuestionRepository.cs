﻿using TestingKnowledge.Model;

namespace TestingKnowledge.Repository
{
    public interface IQuestionRepository : IRepository < Question >
    {
    }
}
