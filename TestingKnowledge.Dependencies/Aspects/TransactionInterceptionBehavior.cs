﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;

using TestingKnowledge.Repository;

namespace TestingKnowledge.Dependencies
{
    class TransactionInterceptionBehavior< TDbContext > : IInterceptionBehavior
        where TDbContext : ITransactionDbContext
    {
        public IMethodReturn Invoke ( IMethodInvocation input, GetNextInterceptionBehaviorDelegate getNext )
        {
            _transactionDbContext.BeginTransaction();

            var result = getNext()( input, getNext );
            if ( result.Exception != null )
                _transactionDbContext.Rollback();

            else
                _transactionDbContext.Commit();

            return result;
        }

        public IEnumerable< Type > GetRequiredInterfaces ()
        {
            return Type.EmptyTypes;
        }

        public bool WillExecute
        {
            get { return true; }
        }

        [ Dependency ]
        protected TDbContext _transactionDbContext { get; set; }
    }
}
