﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Diagnostics.Tracing;

using Microsoft.Practices.EnterpriseLibrary.SemanticLogging;
using Microsoft.Practices.Unity;

namespace TestingKnowledge.Dependencies
{
    public class LogListener : IDisposable
    {
        internal void OnStartup ()
        {
            listenerInfo = FlatFileLog.CreateListener( "services.log" );
            listenerInfo.EnableEvents( Log, EventLevel.LogAlways, TestingKnowledgeEventSource.Keywords.ServiceTracing );

            listenerErrors = FlatFileLog.CreateListener( "diagnostic.log" );
            listenerErrors.EnableEvents( Log, EventLevel.LogAlways, TestingKnowledgeEventSource.Keywords.Diagnostic );

            Log.StartupSucceeded();
        }

        public void Dispose ()
        {
            listenerInfo.DisableEvents( Log );
            listenerErrors.DisableEvents( Log );

            listenerInfo.Dispose();
            listenerErrors.Dispose();
        }


        [ Dependency ]
        protected TestingKnowledgeEventSource Log { get; set; }

        private EventListener listenerInfo;
        private EventListener listenerErrors;
    }
}
