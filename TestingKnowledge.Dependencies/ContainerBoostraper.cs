﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */


using System.Linq;
using System.Reflection;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;
using Microsoft.Practices.EnterpriseLibrary.Validation.PolicyInjection;

using TestingKnowledge.Repository;
using EntityFrameworkDbContext = TestingKnowledge.Repository.EntityFramework.TestingKnowledgeDbContext;
using MemoryDbContext = TestingKnowledge.Repository.Memory.TestingKnowledgeDbContext;

namespace TestingKnowledge.Dependencies
{
    public static class ContainerBoostraper
    {
        public static void RegisterTypes( IUnityContainer container, EntityFrameworkDbContext dbContext )
        {
            container.AddNewExtension< Interception >();

            container.RegisterInstance( dbContext );

            RegisterLogFacilities( container );
            RegisterRepositories( container );
            RegisterServices< EntityFrameworkDbContext >( container );
        }

        public static void RegisterTypesTest( IUnityContainer container, MemoryDbContext dbContext )
        {
            container.AddNewExtension< Interception >();

            container.RegisterInstance( dbContext );
            
            RegisterRepositoriesTest( container );
            RegisterServices< MemoryDbContext >( container );
        }

        private static void RegisterLogFacilities( IUnityContainer container )
        {
            container.RegisterInstance( new TestingKnowledgeEventSource() );
            container.RegisterType(
                typeof( LogListener ),
                new ContainerControlledLifetimeManager()
            );

            var logListener = container.Resolve<LogListener>();
            logListener.OnStartup();
        }

        private static void RegisterServices< TDbConext >( IUnityContainer container )
            where TDbConext : ITransactionDbContext
        {
            container.Configure< Interception >()
                .AddPolicy( "ValidationPolicy" )
                    .AddMatchingRule<NamespaceMatchingRule>(
                        new InjectionConstructor( "TestingKnowledge.Service.Impl", true )
                    )
                    .AddCallHandler(
                        new ValidationCallHandler( "", SpecificationSource.Both )
                    )
                    ;

            container.RegisterTypes(
                AllClasses.FromAssemblies(
                    new Assembly [] {
                        Assembly.Load( "TestingKnowledge.Service.Impl" )
                    }
                ),

                WithMappings.FromMatchingInterface,
                WithName.Default,
                WithLifetime.ContainerControlled,
                getInjectionMembers: t => new InjectionMember []
                {
                    new Interceptor< InterfaceInterceptor >(),
                    new InterceptionBehavior< ExceptionInterceptionBehavior >(),
                    new InterceptionBehavior< SemanticLoggingInterceptionBehavior >(),
                    new InterceptionBehavior< TransactionInterceptionBehavior< TDbConext > >(),
                    new InterceptionBehavior< PolicyInjectionBehavior >( "ValidationPolicy" )
                }
            );
        }

        private static void RegisterRepositories( IUnityContainer container )
        {
            container.RegisterTypes(
                AllClasses.FromAssemblies(
                    new Assembly [] {
                        Assembly.Load( "TestingKnowledge.Repository.EntityFramework" )
                    }
                ).Where( t => t != typeof( EntityFrameworkDbContext ) ),

                WithMappings.FromAllInterfaces,
                WithName.Default,
                WithLifetime.ContainerControlled
            );
        }

        private static void RegisterRepositoriesTest( IUnityContainer container )
        {
            container.RegisterTypes(
                AllClasses.FromAssemblies(
                    new Assembly [] {
                        Assembly.Load( "TestingKnowledge.Repository.Memory" )
                    }
                ),

                WithMappings.FromAllInterfaces,
                WithName.Default,
                WithLifetime.ContainerControlled
            );
        }
    }
}
