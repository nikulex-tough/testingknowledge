﻿using System;
using System.Collections.Generic;
using TestingKnowledge.Dto;

namespace TestingKnowledge.Service
{
    public interface ITestSessionManagementService : ITestSessionCommonService
    {
        IList < TestSessionDto > FindByAccount( Guid accountId );
        IList < TestSessionDto > FindByTest( Guid testId );

        IList < Guid > ViewActive( Guid testId );
        IList < Guid > ViewFinished( Guid testId );

        IList < Guid > ViewPassed( Guid testId, [MarkValidator] float minMark );
        IList < Guid > ViewNotPassed( Guid testId, [MarkValidator] float minMark );
        
        float GetMaxMark( Guid testId );
        float GetAverageMark( Guid testId );
        float GetMinMark( Guid testId );

        void DeleteTestSession( Guid testSessionId );
    }
}
