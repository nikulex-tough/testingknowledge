﻿using System;

namespace TestingKnowledge.Service
{
    public interface ITestSessionService : ITestSessionCommonService
    {
        void AnswerInput( Guid sessionId, Guid questionId, string answer );

        void AnswerCheckState( Guid sessionId, Guid questionId, Guid optionId, bool selected = true );
    }
}
