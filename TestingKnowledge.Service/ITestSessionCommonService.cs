﻿using System;
using System.Collections.Generic;
using TestingKnowledge.Dto;

namespace TestingKnowledge.Service
{
    public interface ITestSessionCommonService : IDomainEntityService < TestSessionDto >
    {
        TestSessionQuestionDto ViewQuestion( Guid sessionQuestionId );

        TestSessionQuestionDto FindQuestion( Guid sessionId, Guid questionId );

        Guid CreateTestSession( Guid accountId, Guid testId );

        void FinishTestSession( Guid testSessionId );

        IList < string > GetRightAnswer( Guid sessionQuestionId );
    }
}
