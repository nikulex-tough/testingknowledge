﻿using System;
using System.Linq;
using TestingKnowledge.Dto;
using TestingKnowledge.Utils.Validators;

namespace TestingKnowledge.Service
{
    public interface ITestEditorService : IDomainEntityService < TestDto >
    {
        IQueryable< Guid > ViewByCreator( Guid accountId );
        TestDto ViewByTitle( string title );

        QuestionGroupDto ViewQuestionGroup( Guid questionGroupId );
        QuestionDto ViewQuestion( Guid questionId );
        OptionDto ViewOption( Guid optionId );

        Guid CreateTest(
                Guid accountId
            ,   [NonEmptyStringValidator] string title
            ,   string description
        );
        void EditTestTitle( Guid testId, [NonEmptyStringValidator] string title );
        void EditTestDescription( Guid testId, string description );
        void DeleteTest( Guid testId );

        Guid CreateQuestionGroup(
                Guid testId
            ,   [MarkValidator] float markOfOneQuestion
        );
        void EditQuestionGroupMarkOfOneQuestion(
                Guid questionGroupId
            ,   [MarkValidator] float markOfOneQuestion
        );
        void EditQuestionGroupCountGenerate(
                Guid questionGroupId
            ,   [CountGenerateValidator] int countGenerate
        );
        void EditQuestionGroupCountGenerateToMax( Guid questionGroupId );
        void DeleteQuestionGroup( Guid questionGroupId );


        Guid CreateQuestionInput(
                Guid questionGroupId
            ,   [NonEmptyStringValidator] string text
            ,   [NonEmptyStringValidator] string rightAnswer
        );
        void EditQuestionInputRightAnswer(
                Guid questionInputId
            ,   [NonEmptyStringValidator] string rightAnswer
        );
        string GetQuestionInputRightAnswer( Guid questionInputId );

        Guid CreateQuestionSelect(
                Guid questionGroupId
            ,   [NonEmptyStringValidator] string text
        );
        Guid CreateQustionOption(
                Guid questionSelectId
            ,   [NonEmptyStringValidator] string text, bool isRight
        );
        void EditOption(
                Guid optionId
            ,   [NonEmptyStringValidator] string text
            ,   bool isRight
        );
        bool IsRightOption( Guid optionId );
        void DeletOption( Guid optionId );

        void EditQuestionText(
                Guid questionId
            ,   [NonEmptyStringValidator] string text
        );
        void DeleteQuestion( Guid questionId );
    }
}
