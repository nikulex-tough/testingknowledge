﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace TestingKnowledge.Service
{
    public interface IDomainEntityService < TDto >
    {
        IList< Guid > ViewAll ();

        TDto View ( Guid domainId );
    }
}
