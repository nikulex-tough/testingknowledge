﻿using System;

using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace TestingKnowledge.Service
{
    [AttributeUsage( AttributeTargets.Property |
                      AttributeTargets.Field |
                      AttributeTargets.Parameter )
    ]
    public class CountGenerateValidator : ValidatorAttribute
    {
        protected override Validator DoCreateValidator( Type targetType )
        {
            return new RangeValidator(
                    0,
                    RangeBoundaryType.Exclusive,
                    int.MaxValue,
                    RangeBoundaryType.Ignore
            );
        }
    }
}