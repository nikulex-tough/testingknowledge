﻿using System;
using TestingKnowledge.Dto;
using TestingKnowledge.Utils.Validators;

namespace TestingKnowledge.Service
{
    public interface IAccountService : IDomainEntityService < AccountDto >
    {
        AccountDto Identify(
                [NonEmptyStringValidator] string login
            ,   [NonEmptyStringValidator] string password
        );

        Guid Create(
                AccountRole role
            ,   [NonEmptyStringValidator] string login
            ,   [NonEmptyStringValidator] string password
        );
        void Delete( Guid accountId );

        void EditRole( Guid accountId, AccountRole role );
        void EditLogin( Guid accountId, [NonEmptyStringValidator] string login );
        void EditPassword(
                Guid accountId
            ,   [NonEmptyStringValidator] string newPassword
        );
    }
}
