﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace TestingKnowledge.Exceptions
{
    public class DuplicateNamedEntityException : DomainLogicException
    {
        public DuplicateNamedEntityException ( Type t, string name )
            :   base( string.Format( $"Duplicate {t.Name} object called \"{name}\"" ) )
        {}
    }
}
