﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace TestingKnowledge.Exceptions
{
    public class DomainLogicException : Exception
    {
        public DomainLogicException ( string message )
            :   base( message )
        {}
    }
}
