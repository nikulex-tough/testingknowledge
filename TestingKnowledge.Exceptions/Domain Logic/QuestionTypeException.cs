﻿using System;

namespace TestingKnowledge.Exceptions
{
    public class QuestionTypeException : DomainLogicException
    {
        public QuestionTypeException( Type should, Type real, Guid questioId )
            :   base( $"Question type error: {real.Name} - should be {should.Name} questionId = {questioId}" )
        {
        }
    }
}
