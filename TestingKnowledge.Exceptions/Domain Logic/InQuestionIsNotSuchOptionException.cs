﻿using System;

namespace TestingKnowledge.Exceptions
{
    public class InQuestionIsNotSuchOptionException : DomainLogicException
    {
        public InQuestionIsNotSuchOptionException( string question, string option )
            :   base( $"In question \"{question}\" is not such option \"{option}\"" )
        {
        }
    }
}
