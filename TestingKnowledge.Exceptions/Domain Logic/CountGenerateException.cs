﻿using System;

namespace TestingKnowledge.Exceptions
{
    public class CountGenerateException : DomainLogicException
    {
        public CountGenerateException( int real, int count )
            :   base( $"Error count generate: value={real} should be less than count of questions - {count}" )
        {
        }
    }
}
